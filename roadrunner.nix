{ buildPythonApplication, pyyaml, lupa, psutil }:

buildPythonApplication {
    pname = "roadrunner";
    version = "0.0.0";

    src = builtins.filterSource
        (path: type: !(baseNameOf path == "playground")) ./.;

    propagatedBuildInputs = [
        pyyaml
        lupa
        psutil
    ];
    doCheck = false;

}