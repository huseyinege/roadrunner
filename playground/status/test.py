#!/usr/bin/env python3

import roadexec.status as st
import time
import threading

def upd():
    status = st.Status("Running")
    start = time.time()
    while True:
        running = time.time() - start
        status.update(f"< {running:.1f} >")
        time.sleep(0.1)

th = threading.Thread(target=upd, daemon=True)
th.start()

print("hallo!")
time.sleep(1.0)
print("Welt")
with st.Status("counter") as status:
    for i in range(10):
        time.sleep(0.5)
        print(i)
        status.update(f"< {i} >")

for i in range(4):
    print("burbel burbel")
    time.sleep(0.9)

print("done!")