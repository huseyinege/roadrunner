import "DPI-C" function void    dpiInit();
import "DPI-C" function int     dpiUpdate();
import "DPI-C" function int     dpiPoll(output int data);
import "DPI-C" function void    dpiPush(input int data);

localparam FL_VAL  = 'h1;
localparam FL_STOP = 'h2;

module Test ();

initial begin
    dpiInit();
end

initial forever begin 
    int status;
    #100 status = dpiUpdate();
    if(status & FL_STOP) begin
        $stop();
    end
end

//reading dpi
initial forever #100 begin
    int value;
    if(dpiPoll(value)) begin
        $display("SV: got an value");
        dpiPush(value);
    end
end

endmodule