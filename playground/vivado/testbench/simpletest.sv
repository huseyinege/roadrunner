///////////////////////////////////////////////
// Vodafone Chair Mobile Communication System
// TU Dresden
// 
// email: mattis.hasler@tu-dresden.de
// authors: Mattis Hasler
// notes:

import aludef::*;

module simpletest;

parameter OP1 = 13;
parameter OP2 = 7;
parameter EXP = 20;


//clock generation
logic clk;
initial clk = 0;
always #5 clk = ~clk;

task clock(int cycles);
    for(int i = 0; i < cycles; i++)
       @(posedge clk);
endtask

snum op1;
snum op2;
snum res;

initial begin
    op1 = OP1;
    op2 = OP2;
end

adder add1 (
    .clk_i(clk),
    .a1_i(op1),
    .a2_i(op2),
    .sum_o(res)
);

initial begin
    int ret;
    clock(2);
    $display("%0d + %0d = %0d (%0d)", op1, op2, res, EXP);
    ret = res == EXP ? 0 : 1;
    clock(1);
    RREnv::RRResult(ret);
    $stop;
end

endmodule