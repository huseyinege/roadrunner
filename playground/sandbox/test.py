#!/usr/bin/env python3

import config
import yaml
import pathlib

pth = pathlib.Path("playground/RR")
cc = config.fromfile(pth)

cc.getval(".unit")
cc.getval(".unit2.unit1")

cc.dump()

def getval(name, mklist=False):
    print(f"GET {name}: {cc.getval(name, mklist=mklist)}")

def getlocation(name):
    node = cc.get(name)
    print(f"LOC {name}: {node.location}")

def name2path(name):
    print(f'{name} -> {config.name2path(name)}')

try:
    name2path(':')
    name2path('.')
    name2path('.unit2.test')
    name2path(':unit.test.')
    name2path(':unit.tets..')
    getval(".unit2.unit1")
    getval(".thing.names")
    getval(".unit.car", mklist=True)
    getval(".unit2.name")
    getlocation(".")
    getlocation(".thing.names.1")
    getlocation(".unit.weapon")
    getlocation(".unit")

except config.ConfigError as ex:
    print("Problem:", str(ex))