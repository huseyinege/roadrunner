set name {name}
set board {board}
set part {part}
set iptype {typ}

create_project -in_memory rrun project

set prj [get_projects rrun]
set_property part $part $prj
#set_property board_part $board $prj
set_property target_language Verilog $prj

create_ip -vlnv $iptype -module_name $name -dir . -force
set ipi [get_ips $name]
set props {{}}

{propset}

set_property -dict $props $ipi

report_property $ipi

generate_target all $ipi

#generate design check point for synthesis
synth_ip $ipi

set prefix [expr [string length [pwd]] + 1]

set p [get_property IP_FILE $ipi]
set xci [string range $p $prefix 1000]

set fh [open RR w]
puts $fh "$name:"
puts $fh "  v:"
foreach fil [get_files -of_object $ipi -filter {{FILE_TYPE == VERILOG}}] {{
    set rel [string range $fil $prefix 1000]
    puts $fh "    - $rel"
}}
puts $fh "  xci: $xci"
puts $fh "  lib: unisim"
