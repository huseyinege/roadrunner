from __future__ import annotations
from roadrunner.config import ConfigNode, ConfigDict, ConfigList, ConfigLeaf, ConfigLink, InvalidName, NoValue, PathNotExist
from pathlib import Path
import roadrunner.rr as rr
import logging
from roadrunner.rr import Pipeline
from roadrunner.fn import etype, clone

logg = logging.getLogger("modFiles")

def helpGatherEnvFiles(owner:str, match:rr.HelpMatch) -> list[rr.HelpPiece]:
    return [
        rr.HelpPiece(owner, rr.HelpType.ATTR, (match, rr.HelpMatch(attr=attr)), attr, desc) for attr, desc in [
            ("env", "(gather) dictionary of values to be defined in an environment file"),
            ("files", "(gather) dictionary of files to be imported to the workdir with the path accessible in the environment file")
        ]
    ]

def gather_env_files(cnf:ConfigNode, wd:Path, tags:list[str]):
    vars = {}
    vars_origin = {}
    #files & env
    for node in rr.travers(cnf, tags):
        #variables
        loc = node.getval(".env", default={})
        for key in loc:
            if key in vars:
                logg.warn(f"env var:{key} shadowed:{vars_origin[key]} by:{node.getname()}")
            vars_origin[key] = node.getname()
        vars.update(loc)
        #files
        for name,itm in node.getval(".files", default={}, path=True).items():
            fil = rr.workdir_import(wd, itm)
            if not isinstance(fil, list):
                fil = [fil]
            if name in vars:
                logg.warn(f"env file:{key} shadowed:{vars_origin[key]} by:{node.getname()}")
            vars_origin[name] = node.getname()
            vars[name] = [str(f) for f in fil] #no posix paths
        #import
        try:
            loc = node.get(".import")
            dest = loc.getval('.dest', default=".")
            base = loc.getval('.base', path=True)
            source = base[0] / base[1]
            for itm in loc.getval('.include', mklist=True):
                logging.getLogger('modFiles').info(f"import:{itm}")
                for fitm in source.glob(itm):
                    logging.getLogger('modFiles').info(f"glob:{fitm}")
                    clone(fitm, wd / dest / fitm.name)
        except PathNotExist:
            pass

    return vars

def bash_val(val) -> str:
    if isinstance(val, list):
        return '(' + " ".join(bash_val(x) for x in val) + ')'
    elif isinstance(val, (str, Path)):
        return '"' + str(val) + '"'
    else:
        return str(val)

def _expose(cnf:ConfigDict|ConfigList|ConfigLeaf|ConfigLink, pipe:Pipeline):
    cnf = cnf.resolve()
    if isinstance(cnf, ConfigDict):
        for name,fnode in cnf.items():
            file = fnode.strip()
            pipe.expose(file, name)
    elif isinstance(cnf, ConfigList):
        for node in cnf:
            _expose(node, pipe)
    elif isinstance(cnf, ConfigLeaf):
        file = cnf.strip()
        pipe.expose(file)
    else:
        raise Exception(f"strange config node type:{type(cnf)} in _expose")

def _discover(cnf:ConfigDict|ConfigList|ConfigLeaf|ConfigLink, pipe:Pipeline):
    cnf = cnf.resolve()
    if isinstance(cnf, ConfigDict):
        for name,fnode in cnf.items():
            file = fnode.strip()
            pipe.discover(file, name)
    elif isinstance(cnf, ConfigList):
        for node in cnf:
            _discover(node, pipe)
    elif isinstance(cnf, ConfigLeaf):
        file = cnf.strip()
        pipe.discover(file)
    else:
        raise Exception(f"strange config node type:{type(cnf)} in _discover")

def helpShare(owner:str, match:rr.HelpMatch) -> list[rr.HelpPiece]:
    return [
        rr.HelpPiece(owner, rr.HelpType.ATTR, match, attr, desc) for attr, desc in [
            ("expose", "Files to be exposed for other RoadRunner commands"),
            ("discover", "Files to be used that come from other RoadRunner commands")
        ]
    ]

def share(cnf:ConfigNode, pipe:Pipeline):
    etype((cnf, ConfigNode), (pipe, Pipeline))
    #expose
    try:
        node = cnf.get(".expose")
        _expose(node, pipe)
    except PathNotExist:
        pass
    #discover
    try:
        node = cnf.get(".discover")
        _discover(node, pipe)
    except PathNotExist:
        pass

