from roadrunner.config import ConfigNode
from pathlib import Path
import roadrunner.rr as rr

DEFAULT_FLAGS = ['PYTHON']
RRENV_FILE = "rrenv.py"

def helpGatherPython(owner:str, match:rr.HelpMatch) -> list[rr.HelpPiece]:
    return [
        rr.HelpPiece(owner, rr.HelpType.ATTR, (match, rr.HelpMatch(attr=attr)), attr, desc) for attr, desc in [
            ("pymod", "(gather) python module that will be accessable through an python path")
        ]
    ]

def gather_python(cnf:ConfigNode, wd:Path, tags:list[str], flags:list[str], raw:bool=False) -> tuple[list[Path], set[str]]:
    dd = rr.gather(cnf, tags, ['pymod'], flags, path=True, raw=raw)
    paths = set()

    return dd['pymod']

def python_vars(vars:dict) -> str:
    data = []
    for name,val in vars.items():
        data.append(f"{name}={val.__repr__()}")
    return "\n".join(data)

