from roadrunner.rr import Pipeline
from pathlib import Path
import roadrunner.rr as rr


NAME = "sv2v"

def configDefault(pipe:Pipeline):
    pipe.configDefault(NAME, 'bin', 'sv2v $@')

#converts already imported .sv files to .v
def convertFiles(wd:Path, pipe:Pipeline, files:list[Path]|Path,
    defines:list[str]=[], includes:list[str]=[]
):
    single = False
    if not isinstance(files, list):
        single = True
        files = [files]
    converted = {}
    call = rr.Call(wd, 'sv2v', tool=(NAME, 'bin'))
    call.addArgs(['--write', 'adjacent'])
    for fname in files:
        call.addArgs([fname])
        converted[fname] = fname.with_suffix('.v')
    for inc in includes:
        call.addArgs(["-I", inc])
    for d in defines:
        call.addArgs(["-D", d])
    pipe.runCall(call)
    return converted



