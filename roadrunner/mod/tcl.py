def tcl_val(val):
    if isinstance(val, list):
        return " ".join([f"{{{x}}}" for x in val])
    elif val is None:
        return "{}"
    else:
        return f"{{{val}}}"

