from pathlib import Path
import roadrunner.rr as rr
from roadrunner.config import ConfigNode
from dataclasses import dataclass

RRENV_FILE = "RREnv.sv"

def write_env_file(loc:Path, vars:dict) -> Path:
    fname = loc / RRENV_FILE
    params = []
    for name, var in vars.items():
        params.append(f"localparam {name} = {sv_val(var)};")
    with open(fname, "w") as fh:
        tpl = rr.template('verilog/RREnv.sv')
        print(tpl.format(lparams="\n".join(params)), file=fh)
    return fname

def sv_val(val):
    if isinstance(val, list):
        return "'{" + ", ".join(sv_val(x) for x in val) + "}"
    elif isinstance(val, (str, Path)):
        return '"' + str(val) + '"'
    else:
        return str(val)

@dataclass
class FileItem:
    typ:str
    file:Path
    defines:list[str]
    includes:list[Path]

def helpGatherVerilog(owner:str, match:rr.HelpMatch) -> list[rr.HelpPiece]:
    return [
        rr.HelpPiece(owner, rr.HelpType.ATTR, (match, rr.HelpMatch(attr=attr)), attr, desc) for attr, desc in [
            ("sv", "(gather) Systemverilog files to be used"),
            ("v", "(gather) Verilog files to be used"),
            ("vhdl", "(gather) VHDL files to be used"),
            ("define", "(gather) dictionary of defines for the source files"),
            ("path", "(gather) include path for source files")
        ]
    ]

def gather_verilog(cnf:ConfigNode, wd:Path, tags:list[str], flags:dict[str,bool],
    defaultSimDefines:list[str]=[]) -> list[FileItem]:
    pathAttr = {
        'path': True,
        'sv': True,
        'v': True,
        'vhdl': True,
        'define': False
    }
    gath = rr.gather(cnf, tags, pathAttr.keys(), flags, path=pathAttr, united=False)

    files = []
    for itm in gath:
        defs = itm['define'] + defaultSimDefines
        incs = []
        for inc in rr.workdir_import(wd, itm['path']):
            if inc.parent not in incs:
                incs.append(inc.parent)
        files += [FileItem('sv', fname, defs, incs) for fname in rr.workdir_import(wd, itm['sv'])]
        files += [FileItem('verilog', fname, defs, incs) for fname in rr.workdir_import(wd, itm['v'])]
        files += [FileItem('vhdl', fname, defs, incs) for fname in rr.workdir_import(wd, itm['vhdl'])]

    #remove duplicates
    ret = []
    for itm in files:
        if itm not in ret:
            ret.append(itm)

    return ret
