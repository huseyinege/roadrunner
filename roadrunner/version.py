import logging
import re
import subprocess
import sys

import roadrunner.fn

# major.minor.patch
# major - not backwards compatible changes
# minor - backwards compatible changes, features extension
# patch - bug fixes only
# -dev  - not hitting a release label exactly - meaning somewhere between versions

REX_GITVERSION = r'(v[\d\.]+)(-[\w-]+)?'

version_const = "v2.0.2"

def version_string():
    label, isdev = gitlabel()
    if label is not None and label.split('.')[0:2] != version_const.split('.')[0:2]:
        logging.getLogger('RR').warning("git label does not match const version")
    return version_const + ('-dev' if isdev else '')

def gitlabel():
    try:
        raw = subprocess.check_output(["git", "describe", "--tags"], stderr=subprocess.DEVNULL, cwd=roadrunner.fn.getroot()).decode(sys.stdout.encoding).strip()
    except FileNotFoundError: #git not installed?
        return None, False
    except subprocess.CalledProcessError: #not a git repository
        return None, False
    m = re.match(REX_GITVERSION, raw)
    isdev = False if m.group(2) is None else True
    return m.group(1), isdev
        