#!/usr/bin/env python3

import argparse
import logging
import os
import pathlib
import re

import roadrunner.config as config
from roadrunner.config import ConfigList, PathNotExist
import roadrunner.fn as fn
import roadrunner.tools as tools
import roadrunner.nativefn
import roadrunner.rr as rr

CONFIG = {
    "_setup" : {
        "workdir_base": 'rrun',
        "result_base": 'rres'
    },
    "_run" : {
        "command": None,
        "args": []
    }
}

#todo maybe remove confif files here
CONFIG_FILES = [
    "/etc/roadrunner/setup.yaml",
    "~/.config/roadrunner/setup.yaml"
]

def main():
    #load command line arguments
    cli = parseargs()
    #set loglevels
    logging.basicConfig(level=logging.INFO, format="%(levelname)-8s| %(message)s")
    if 'log' in cli:
        for scope, level in cli['log'].items():
            logging.getLogger(scope).setLevel(level)
    #start up Roadrunner
    logg = logging.getLogger("roadrunner")
    logg.info(fn.banner(f"RoadRunner {fn.getversion()}"))
    #chdir?
    if 'dir' in cli and cli['dir'] != '.':
        logging.getLogger('rr').info(f"change dir:{cli['dir']}")
        os.chdir(cli['dir'])
    #config
    cnf = makeconfig()
    #merge cli
    clicnf = config.makeConfigVal({
        '_run': cli
    })
    cnf.merge(clicnf)
    #run
    nocatch = cnf.getval(':_run.dontcatch', default=False)
    try:
        run(cnf)
    except config.ConfigError as ex:
        print(ex)
        if nocatch:
            raise
    logg.info(fn.banner(f"RoadRunner", False))

def makeconfig():
    #defaut config
    cnf = config.makeConfigVal(CONFIG)
    #load and merge global config files
    for fname in CONFIG_FILES:
        pth = pathlib.Path(fname).expanduser()
        if not pth.exists():
            continue
        add = config.fromfile(pth)
        cnf.merge(add)
    #load RR
    try:
        add = config.fromfile(pathlib.Path('RR'))
        cnf.merge(add)
    except FileNotFoundError:
        logging.getLogger('rr').warning("RR file not found - only global config loaded")
    #add natives
    dd = roadrunner.nativefn.buildnatives()
    cnf.setval(":_setup.natives", dd, create=True)
    #
    return cnf
            
def parseargs():
    parser = argparse.ArgumentParser()
    parser.add_argument('--dir', type=str, help="change to this directory before doing anything")
    parser.add_argument('--dontcatch', action="store_true", help='disables the catchall exception')
    parser.add_argument('--log', '-l', type=str, action="append", help="set loglevels with <scope>=<level>")
    parser.add_argument('query', type=str, default="query", help='query to do, can be omitted, then "run" subsided')
    parser.add_argument('params', type=str, nargs=argparse.REMAINDER, help='parameters for the query')
    args = parser.parse_args()
    run = {
        "query": args.query,
        "args": args.params
    }
    if args.dir is not None:
        run['dir'] = args.dir
    if args.dontcatch:
        run['dontcatch'] = True
    if args.log is not None:
        run['log'] = {}
        for cmd in args.log:
            scope, level = loggingParse(cmd)
            run['log'][scope] = level
    return run

def loadresults(cnf):
    loc, rel = cnf.getval(':_setup.result_base', path=True)
    base = rel if loc is None else loc / rel
    if not base.is_dir():
        return
    cnf.set(":res", config.makeConfigVal({}))
    for wd in base.iterdir():
        if not wd.is_dir() or not (wd / 'RR').exists():
            continue
        name = str(wd.relative_to(base))
        cnf.set(f":res.{name}", config.makeConfigVal("+" + name, location=base), create=True)

REX_LOGGING = r'((\w+)\s*=)?\s*(\w+)'
REX_LEVEL = r'(\w+)(\+(\d+))?'

def loggingLevelInt(val:str|int) -> int:
    if isinstance(val, int):
        return val
    if isinstance(val, str):
        try:
            return int(val)
        except ValueError:
            pass
        m = re.match(REX_LEVEL, val)
        if m is None:
            raise Exception(f"cannot parse log level:{val} from string")
        name = m.group(1).lower()
        offstr = m.group(3)
        if offstr is not None:
            offset = int(offstr)
        else:
            offset = 0
        if name in ['debug', 'dbg', 'd']:
            base = logging.DEBUG
        elif name in ['info', 'i']:
            base = logging.INFO
        elif name in ['warn', 'warning', 'w']:
            base = logging.WARNING
        elif name in ['err', 'error', 'e']:
            base = logging.ERROR
        elif name in ['crit', 'critical', 'c']:
            base = logging.CRITICAL
        else:
            raise Exception(f"unknown logging level name:{name}")
        return base + offset
    raise Exception(f"cannot parse log level:{val} from:{type(val)}")

def loggingParse(itm:str) -> tuple[str, int]:
    log = logging.getLogger('roadrunner')
    m = re.match(REX_LOGGING, itm)
    if m is None:
        log.warn(f"cannot parse logging directive:{itm}")
        return None
    scope = m.group(2)
    if scope is None:
        scope = "root"
    level = loggingLevelInt(m.group(3))
    
    return scope, level

def run(cnf):
    #load tools
    tools.loadtools(cnf)
    #load results
    loadresults(cnf)
    #print the to be run command
    try:
        query = tools.getquery(cnf.getval(':_run.query'))
    except tools.ToolException:
        logging.getLogger('RR').info("no query specified, defaulting to 'invoke'")
        params = cnf.getval(':_run.query',mklist=True) + cnf.getval(':_run.args', mklist=True)
        params[0] = ':' + params[0]
        cnf.setval(':_run.args', params)
        cnf.setval(':_run.query', 'invoke')
        query = tools.getquery(cnf.getval(':_run.query'))
    #run
    query(cnf)

if __name__ == '__main__':
    main()