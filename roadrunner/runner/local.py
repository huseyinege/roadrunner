from roadrunner.config import ConfigNode
from pathlib import Path
import os

def run(cmd:ConfigNode, dir:Path, script:Path):

    od = os.getcwd()
    glob = {'args':{
        'sharesFile': "../shares.yaml",
        'resultDir': "../../rres", #TODO derive from result_base
        'dontParseArgs': True
    }}
    #enter exec stage
    os.chdir(dir)
    exec(open(script).read(), glob)
    #end
    os.chdir(od)