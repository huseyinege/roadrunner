from __future__ import annotations
import logging
import threading
from roadrunner.config import ConfigList, ConfigNode, PathNotExist
import roadrunner.fn as fn
import roadrunner.rr as rr
from roadrunner.rr import Pipeline
from enum import Enum
from roadrunner.rr import HelpPiece, HelpType, HelpMatch

NAME = "Runner"
DESCRIPTION = "Roadrunner Multi Command Runner"

class RunType(str,Enum):
    parallel = "parallel"
    single = "single"
    sequence = "sequence"
    pool = "pool"

class RunCmd:
    def __init__(self, mode:RunType, cmds:list[RunCmd] | ConfigNode):
        self.mode = mode
        self.cmds = cmds
        self.thread = None

    def run(self, group:rr.CommandGroup):
        try:
            if self.mode == 'sequence':
                for cmd in self.cmds:
                    cmd.start(group)
                    group.waitEmpty()
            elif self.mode == 'parallel':
                for cmd in self.cmds:
                    cmd.start(group)
            elif self.mode == 'single':
                cmd = rr.Command(self.cmds, group)
                cmd.start()
            elif self.mode == 'pool':
                for cmd in self.cmds:
                    cmd.start(group)
                    with group.cmdCond:
                        group.cmdCond.wait_for(lambda: len(group) < 1)
            else:
                raise Exception(f"unknown RunCmd mode: {self.mode}")
        except rr.CommandGroupTearing:
            pass
        finally:
            group.waitEmpty()
            group.deactivate()

    def start(self, parent:rr.CommandGroup):
        group = rr.CommandGroup(parent)
        group.activate()
        self.thread = threading.Thread(
            target=self.run, args=(group, ), name=f"RunCmd"
        )
        self.thread.start()


def help() -> list[HelpPiece]:
    pieces = [
        HelpPiece(NAME, HelpType.TOOL, HelpMatch(tool=NAME), NAME, "Batch Runner"),
        HelpPiece(NAME, HelpType.SUBTOOL, HelpMatch(tool=NAME), "run", "default subtool"),
        HelpPiece(NAME, HelpType.SUBTOOL, HelpMatch(tool=NAME), "parallel", "run child in parallel"),
        HelpPiece(NAME, HelpType.SUBTOOL, HelpMatch(tool=NAME), "single", "run a single command (not supported yet)"),
        HelpPiece(NAME, HelpType.SUBTOOL, HelpMatch(tool=NAME), "sequence", "run commands one after another"),
        HelpPiece(NAME, HelpType.SUBTOOL, HelpMatch(tool=NAME), "pool", "run commands independently (not supported yet)"),
        HelpPiece(NAME, HelpType.ATTR, HelpMatch(tool=NAME, subtool="run"), "group", "execution mode. One of: parallel, single, sequence, pool"),
    ]
    return pieces

def cmd_run(cnf:ConfigNode, pipe:Pipeline):
    run(cnf, pipe, None)

def cmd_parallel(cnf:ConfigNode, pipe:Pipeline):
    run(cnf, pipe, RunType.parallel)

def cmd_sequence(cnf:ConfigNode, pipe:Pipeline):
    run(cnf, pipe, RunType.sequence)

def cmd_pool(cnf:ConfigNode, pipe:Pipeline):
    run(cnf, pipe, RunType.pool)

def cmd_single(cnf:ConfigNode, pipe:Pipeline):
    run(cnf, pipe, RunType.single)

def run(cnf:ConfigNode, pipe:Pipeline, type:RunType):
    log = logging.getLogger("runner")
    log.info(fn.banner("Runner"))
    rr.workdir_init(pipe.cwd())
    scan(cnf, pipe, type)

def scan(cnf:ConfigNode, pipe:Pipeline, type:RunType=None):
    log = logging.getLogger("runner")
    log.info(f"{cnf.getname()} start scan")
    # is it a command node (and not a 'Runner' command)
    tool = cnf.getval('.tool', default=NAME)
    if tool not in  [NAME] + [f"{NAME}.{x}" for x in RunType]:
        log.info(f"{cnf.getname()} is leaf")
        rr.command_run(cnf, pipe)
        return

    # what kind of runner is this
    #from function param
    mode = type 
    #from toolname
    toolDesc = tool.split('.')
    if len(toolDesc) > 1:
        mode = RunType(toolDesc[1])

    #from group attribute
    group = cnf.getval(".group", default=None)
    if group is not None:
        mode = RunType(group)

    if mode is None:
        raise Exception(f"no Runner mode in {cnf.getname()}")

    log.info(f"{cnf.getname()} typ is:{mode}")

    #create the group
    pipe.groupEnter(mode)

    #descend
    for key,node in cnf.resolve().items():
        #ignore magic
        if key in ['group', 'tool']:
            continue
        pipe.groupItem(key)
        scan(node, pipe, mode)

    #finish this group
    pipe.groupLeave()

