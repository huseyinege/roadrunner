from roadrunner.config import ConfigNode
from roadrunner.rr import Pipeline
import roadrunner.mod.verilog
import roadrunner.mod.files
import roadrunner.mod.sv2v
import roadrunner.rr as rr
import roadrunner.fn as fn
import logging
from pathlib import Path
from roadrunner.rr import HelpPiece, HelpType, HelpMatch

NAME = "Yosys"

DEFAULT_SIM_FLAGS = ['SIMULATION', 'YOSYS', 'CXXRTL']

CXXRTL_SCRIPT_TEMPL = "yosys/cxxrtl.ys"

def help() -> list[HelpPiece]:
    pieces = [
        HelpPiece(NAME, HelpType.TOOL, HelpMatch(tool=NAME), NAME, "Yosys Open SYnthesis Suite"),
        HelpPiece(NAME, HelpType.SUBTOOL, HelpMatch(tool=NAME), "run", "run script"),
        HelpPiece(NAME, HelpType.SUBTOOL, HelpMatch(tool=NAME), "sim", "run a CXXRTL based simulation")
    ]
    owner = NAME + ".sim"
    match = HelpMatch(tool=NAME, subtool="sim")
    pieces += [
        HelpPiece(owner, HelpType.ATTR, match, attr, desc) for attr,desc in [
            ("toplevel",    "the module that is to be simulated"),
            ("flags",       "given to the gatherer"),
            ("debug",       "debug level to run CXXRTL (0-4)"),
            ("optimize",    "optimization level for CXXRTL (0-6)")
        ]
    ]
    pieces += [
        HelpPiece(owner, HelpType.ATTR, (match, HelpMatch(attr=attr)), attr, desc) for attr,desc in [
            ("include",     "standard flag for the gatherer"),
            ("inc",         "standard flag for the gatherer"),
            ("c",           "C/C++ files to be used"),
            ("cpath",       "include path for C/C++"),
            ("clib",        "libraries for C/C++"),
            ("clibpath",    "library include path for C/C++"),
            ("cstd",        "control the C standard flag")
        ]
    ]
    pieces += roadrunner.mod.verilog.helpGatherVerilog(owner, match)
    pieces += roadrunner.mod.files.helpShare(owner, match)
    pieces += roadrunner.mod.files.helpGatherEnvFiles(owner, match)
    owner = NAME + ".run"
    match = HelpMatch(tool=NAME, subtool="run")
    pieces += [
        HelpPiece(owner, HelpType.ATTR, match, attr, desc) for attr,desc in [
            ("scriptFile", "file to contain the script to be run, cannot be combined with 'script'"),
            ("script", "script content to be run, cannot be combined with 'scriptFile'"),
        ]
    ]
    pieces += [
        HelpPiece(owner, HelpType.ATTR, (match, HelpMatch(attr=attr)), attr, desc) for attr,desc in [
            ("inc", "default tag for gatherer"),
            ("include", "default tag for gatherer")
        ]
    ]
    pieces += roadrunner.mod.verilog.helpGatherVerilog(owner, match)
    pieces += roadrunner.mod.files.helpGatherEnvFiles(owner, match)
    pieces += roadrunner.mod.files.helpShare(owner, match)
    return pieces

def cmd_run(cnf:ConfigNode, pipe:Pipeline):
    log = logging.getLogger('yosys')
    wd = rr.workdir_init(pipe.cwd())

    pipe.configDefault(NAME, 'bin', 'yosys $@')

    roadrunner.mod.files.share(cnf, pipe)

    vars = roadrunner.mod.files.gather_env_files(cnf, wd, [])
    envfile = roadrunner.mod.verilog.write_env_file(wd, vars)

    do_readVerilogScript(cnf, wd, pipe, addFiles=[fn.relpath(envfile, wd)])

    scriptFile = rr.workdir_import(wd, cnf.getval(".scriptFile", path=True, default=None, assertSingle=True))
    scriptInline = cnf.getval(".script", default=None)

    if scriptInline is not None:
        with open(wd / "inline.ys", "w") as fh:
            origin = cnf.get(".script").origin
            print(f"# Inline Script ({origin})", file=fh)
            print(scriptInline, file=fh)
    
    script = "inline.ys" if scriptInline else scriptFile

    call = rr.Call(wd, 'yosys', tool=(NAME, 'bin'))
    call.addArgs([script])
    pipe.runCall(call)


def cmd_sim(cnf:ConfigNode, pipe:Pipeline):
    log = logging.getLogger('yosys')
    wd = rr.workdir_init(pipe.cwd())

    pipe.configDefault(NAME, 'bin', 'yosys $@')
    pipe.configDefault(NAME, 'config', 'yosys-config $@')
    pipe.configDefault("GCC", 'g++', 'g++ $@') #TODO hack
    
    roadrunner.mod.files.share(cnf, pipe)
    vars = roadrunner.mod.files.gather_env_files(cnf, wd, [])
    envfile = roadrunner.mod.verilog.write_env_file(wd, vars)

    do_readVerilogScript(cnf, wd, pipe, addFiles=[fn.relpath(envfile, wd)])

    do_cxxrtl(cnf, pipe)

    do_cxxbuild(cnf, pipe)

    with open(wd / "calls/sim.sh", "w") as fh:
        print("./sim", file=fh)
    pipe.run("sim", "calls/sim.sh")

def do_readVerilogScript(cnf:ConfigNode, wd:Path, pipe:Pipeline, addFiles:list=[]):
    log = logging.getLogger('yosys')
    flags = cnf.getval('.flags', mklist=True, default=[])
    tags = flags + ['include', 'inc']
    files = roadrunner.mod.verilog.gather_verilog(cnf, wd, tags, flags, DEFAULT_SIM_FLAGS)

    roadrunner.mod.sv2v.configDefault(pipe)

    #sv2v
    svfiles = addFiles[:] # start with additional files
    defines = set()
    includes = set()
    for item in files:
        if item.typ == 'sv':
            svfiles.append(item.file)
        for d in item.defines:
            defines.add(d)
        for i in item.includes:
            includes.add(i)
    converted = roadrunner.mod.sv2v.convertFiles(wd, pipe, svfiles, defines, includes)

    with open(wd / "verilogFiles.ys", "w") as fh:
        for item in files:
            if item.typ in ['sv', 'verilog']:
                line = f"read_verilog"
            elif item.typ == 'vhdl':
                log.warning("vhdl is not supported by the Yosys tool wrapper")
            #if item.typ == 'sv':
            #    line += " -sv"
            for d in item.defines:
                line += f" -D{d}"
            for i in item.includes:
                line += f" -I{i}"
            if item.typ == 'sv':
                line += " " + str(converted[item.file])
            else:
                line += " " + str(item.file)
            print(line, file=fh)


def do_cxxrtl(cnf:ConfigNode, pipe:Pipeline):
    log = logging.getLogger('cxxrtl')

    toplevel = cnf.getval(".toplevel")
    debuglevel = cnf.getval(".debug", default="3") #it seems cxxrtl dows not wrk with debug level 4 which is default
    optlevel = cnf.getval(".optimize", default=None)
    wd = pipe.cwd()

    opts = ""
    if debuglevel is not None:
        opts += f" -g{int(debuglevel)}"
    if optlevel is not None:
        opts += f" -O{int(optlevel)}"
    temp = rr.template(CXXRTL_SCRIPT_TEMPL)
    with open(wd / "cxxrtl.ys", "w") as fh:
        print(temp.format(toplevel=toplevel, options=opts), file=fh)

    call = rr.Call(wd, 'yosys', tool=(NAME, 'bin'))
    call.addArgs(['cxxrtl.ys'])
    pipe.runCall(call)

def do_cxxbuild(cnf:ConfigNode, pipe:Pipeline):
    log = logging.getLogger('cxxbuild')
    wd = pipe.cwd()
    flags = cnf.getval('.flags', mklist=True, default=[]) + DEFAULT_SIM_FLAGS
    tags = flags + ['include', 'inc']
    attrs = {'c': True, 'cpath': True, 'clib': False, 'clibpath': True,
        'cstd':False
    }
    lst = rr.gather(cnf, tags, attrs.keys(), flags, path=attrs, united=True)

    toplevel = cnf.getval(".toplevel")

    sources = rr.workdir_import(wd, lst['c'])
    paths = rr.workdir_import(wd, lst['cpath'])
    paths.append('.') #to include cxxrtl

    libs = lst['clib']
    libpaths = rr.workdir_import(wd, lst['clibpath'])

    if len(lst['cstd']):
        std = lst['cstd'][0]
        for x in lst['cstd'][1:]:
            if x != std:
                log.warning("multiple different C standards defined - using first found")
    else:
        std = None

    pipe.loadtool(NAME, "config")

    call = rr.Call(wd, "gcc", tool=("GCC", "g++"))
    call.addArgs(sources)
    for path in paths:
        call.addArgs([f'-I{path}'])
    for lib in libpaths:
        call.addArgs([f'-L{lib}'])
    for lib in libs:
        call.addArgs([f'-l{lib}'])
    if std is not None:
        call.addArgs([f'-std={std}'])
    call.addArgs(['-I`tools/Yosys.config.sh --datdir`/include'])
    call.addArgs(['-g', '-O1'])
    call.addArgs([f'-DSICO_TOPLEVEL={toplevel}'])
    call.addArgs(['-o', 'sim'])

    pipe.runCall(call)
