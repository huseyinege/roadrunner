import logging
import roadrunner.fn as fn
import roadrunner.rr as rr
from roadrunner.config import PathNotExist, path2name, issubtree
import pathlib
import roadrunner.mod.files
import roadrunner.mod.python
import roadrunner.mod.verilog

NAME = "ICPro"
DESCRIPTION = "icpro project exporter"

UNIT_VERILOG_PATH = "units/{unit}/source/rtl/verilog"
UNIT_SVERILOG_PATH = "units/{unit}/source/rtl/systemverilog"
UNIT_VHDL_PATH = "units/{unit}/source/rtl/vhdl"
UNIT_INCLUDE_PATH = "global_src/verilog"
UNIT_C_PATH = "units/{unit}/source/behavioral/c"
UNIT_PYTHON_PATH = "global_src/python/{unit}"

XCELIUM_TESTBENCH_DIR = "units/{unit}/simulation/xcelium/{testbench}"
#scope: ({unit}|{testbench})
#typ: (net|rtl)
#impl: (tb|design|behavioral)
XCELIUM_FILELIST = "units/{unit}/simulation/xcelium/{testbench}/sources/{scope}.{typ}.{impl}.f"

NCSIM_SOURCES_TEMPLATE = "icpro/ncsim_rtl.Makefile"
NCSIM_SOURCES_FILE = "Makefile.rtl.sources"
NCSIM_RRVARS_SV_FILE = "RRVars.sv"
NCSIM_RRVARS_PY_FILE = "rrvars.py"
NCSIM_TESTCASE_DIR = "units/{unit}/simulation/ncsim/{testcase}"
NCSIM_VARS_FILE = "Makefile.var"
NCSIM_VARS_TEMPLATE = "icpro/ncsim_var.Makefile"

DEFAULT_XCELIUM_DEFINE = ["SIMULATION", "RACYICS"]
DEFAULT_NCSIM_DEFINE = ["SIMULATION", "RACYICS"]
DEFAULT_GENUS_DEFINE = ["SYNTHESIS", "RACYICS"]

DEFAULT_XCELIUM_FLAGS = ["SIMULATION", "RACYICS", "ICPRO"]
DEFAULT_NCSIM_FLAGS = ["SIMULATION", "RACYICS", "ICPRO"]
DEFAULT_GENUS_FLAGS = ["SYNTHESIS", "RACYICS", "ICPRO"]
DEFAULT_PYTHON_FLAGS = ["RACYICS", "ICPRO"]

logg = logging.getLogger("icpro")


# IUS generaion
#        print("#injects global defines into different tool setup files", file=fh)
#        print("#IUS", file=fh)
#        defs = [x.strip() for x in dd['define']]
#        opts = " ".join(f"-define {x}" for x in (defs + DEFAULT_IUS_DEFINE))
#        print(f"sed -e 's/DEFINE_OPT\s\+=.*/DEFINE_OPT     = {opts}/' -i env/ius/Makefile.project.var", file=fh)


def cmd_run(cnf):
    st = fn.status(f"{rr.command_name(cnf)}:icpro")
    fn.banner("ICPro")

    wd = rr.workdir(cnf)

    #unit file sorting
    paths = []
    for name,node in cnf.get('.units').items():
        path = node.resolve().getpath()
        paths.append((path,name))

    #genus 
    for unit,ucnf in cnf.get('.genus').items():
        genus_unit(wd, unit, ucnf, paths)

    #xcelium
    try:
        for unit,ucnflst in cnf.get('.xcelium').items():
            for test,ucnf in ucnflst.items():
                xcelium_test(wd, unit, test, ucnf, paths)
    except PathNotExist:
        pass

    #ncsim
    for unit,ucnf in cnf.get('.ncsim').items():
        ncsim_unit(wd, unit, ucnf, paths)

    fn.banner("/ICPro")


def genus_unit(wd, unit, cnf, paths):

    fn.banner(f"ICPRO - Genus - unit:{unit}")

    flags = cnf.getval('.flags', mklist=True, default=[]) + DEFAULT_GENUS_FLAGS
    logg.info(f"flags:{flags}")
    tags = flags + ['include', 'inc']
    attrs = {'path': True, 'sv': True, 'v': True, 'vhdl': True, 'define': False, 'options': False}

    dd = rr.gather(cnf, tags, attrs.keys(), flags, path=attrs, united=True, raw=True)

    #gather files
    files_sv = export_files(wd, dd['sv'], paths, UNIT_SVERILOG_PATH)
    files_v = export_files(wd, dd['v'], paths, UNIT_VERILOG_PATH)
    files_vhdl = export_files(wd, dd['vhdl'], paths, UNIT_VHDL_PATH)
    export_files(wd, dd['path'], paths, UNIT_INCLUDE_PATH)

    #write defines file
    with open(wd / "imp_defines.sh", "a") as fh:
        defs = [x.strip() for x in dd['define']]
        opts = " ".join(defs + DEFAULT_GENUS_DEFINE)
        print(f"#GENUS for {unit}", file=fh)
        print(f"sed -e 's#set_db / \.hdl_verilog_defines\s\.+#set_db / .hdl_verilog_defines \"{opts}\"#' -i units/{unit}/rtl2gds/genus/genus_setup.tcl", file=fh)

    #write filelist file
    fname = wd / "units" / unit / f"rtl2gds/genus/addons/{unit}.design.tcl"
    fname.parent.mkdir(parents=True, exist_ok=True)
    with open(fname, "w") as fh:
        print("set HDL_SEARCH_PATHS [list \\\n\t$ICPRO_DIR/global_src/verilog \\", file=fh)
        print("]\n\nset VERILOG_SRC_LIST [list \\", file=fh)
        for fname in files_v:
            print(f"\t$ICPRO_DIR/{fname} \\", file=fh)
        print("]\n\nset SYSVERILOG_SRC_LIST [list \\", file=fh)
        for fname in files_sv:
            print(f"\t$ICPRO_DIR/{fname} \\", file=fh)
        print("]\n\nset VHDL_SRC_LIST [list \\", file=fh)
        for fname in files_vhdl:
            print(f"\t$ICPRO_DIR/{fname} \\", file=fh)
        print("]", file=fh)

    fn.banner(f"ICPRO - /Genus - unit:{unit}")

def xcelium_test(wd, unit, test, cnf, paths):
    fn.banner(f"ICPRO - XCelium - unit:{unit} test:{test}")

    flags = cnf.getval('.flags', mklist=True, default=[]) + DEFAULT_XCELIUM_FLAGS
    logg.info(f"flags:{flags}")
    tags = flags + ['include', 'inc']
    attrs = {'path': True, 'sv': True, 'v': True, 'vhdl': True, 'define': False, 'options': False}

    dd = rr.gather(cnf, tags, attrs.keys(), flags, path=attrs, united=True, raw=True)

    #gather files
    files_sv = export_files(wd, dd['sv'], paths, UNIT_SVERILOG_PATH)
    files_v = export_files(wd, dd['v'], paths, UNIT_VERILOG_PATH)
    files_vhdl = export_files(wd, dd['vhdl'], paths, UNIT_VHDL_PATH)
    export_files(wd, dd['path'], paths, UNIT_INCLUDE_PATH)

    #toplevel
    toplevel = cnf.getval('.toplevel')
    logg.info(f"TB toplevel:{toplevel}")

    #files & vars
    vars = {}
    for name,itm in cnf.getval(".env", default={}).items():
        vars[name] = itm
    for name,itm in cnf.getval(".files", default={}, path=True).items():
        fil = rr.workdir_import(wd, itm, dirnames=XCELIUM_TESTBENCH_DIR.format(unit=unit, testbench=toplevel))
        vars[name] = fil

    #filelist
    flistname = wd / XCELIUM_FILELIST.format(unit=unit, testbench=toplevel, scope=unit, typ='rtl', impl='design')
    flistname.parent.mkdir(parents=True, exist_ok=True)
    with open(flistname, "w") as fh:
        for fname in files_v:
            print(f"$ICPRO_DIR/{fname}", file=fh)
        for fname in files_sv:
            print(f"$ICPRO_DIR/{fname}", file=fh)
        for fname in files_vhdl:
            print(f"$ICPRO_DIR/{fname}", file=fh)

    fn.banner(f"ICPRO - /XCelium - unit:{unit} test:{test}")

def ncsim_unit(wd, unit, cnf, paths):
    fn.banner(f"ICPRO - NCSim - Unit:{unit}")

    #Testcases
    for tcName, tcCnf in cnf.items():
        ncsim_testcase(wd, unit, tcName, tcCnf, paths)

    fn.banner(f"ICPRO - NCSim - /Unit:{unit}")

def sv_val(val):
    if isinstance(val, list):
        return "'{" + ", ".join(sv_val(x) for x in val) + "}"
    elif isinstance(val, (str, pathlib.Path)):
        return '"' + str(val) + '"'
    else:
        return str(val)

def ncsim_testcase(wd, unit, testcase, cnf, paths):
    fn.banner(f"ICPRO - NCSim - Unit:{unit} - Testcase:{testcase}")

    td = wd / NCSIM_TESTCASE_DIR.format(unit=unit, testcase=testcase)
    td.mkdir(parents=True, exist_ok=True)
    logg.info(f"testcasedir: {td}")
    rtd = fn.relpath(td, wd)

    #### hardware
    hwcnf = cnf.get(".hw")

    flags = hwcnf.getval('.flags', mklist=True, default=[]) + DEFAULT_NCSIM_FLAGS
    logg.info(f"flags:{flags}")
    tags = flags + ['include', 'inc']
    attrs = {'path': True, 'sv': True, 'v': True, 'vhdl': True, 'define': False,
      'options': False, 'c': True, 'cpath': True, 'clib': False,
      'clibpath': True, 'cstd': False
    }

    dd = rr.gather(hwcnf, tags, attrs.keys(), flags, path=attrs, united=True, raw=True)

    #gather files
    files_sv = export_files(wd, dd['sv'], paths, UNIT_SVERILOG_PATH)
    files_v = export_files(wd, dd['v'], paths, UNIT_VERILOG_PATH)
    files_vhdl = export_files(wd, dd['vhdl'], paths, UNIT_VHDL_PATH)
    files_path = export_files(wd, dd['path'], paths, UNIT_INCLUDE_PATH)
    files_c = export_files(wd, dd['c'], paths, UNIT_C_PATH)

    #toplevel
    toplevel = hwcnf.getval('.toplevel')
    logg.info(f"TB toplevel:{toplevel}")

    #files & vars
    vars = roadrunner.mod.files.gather_env_files(hwcnf, wd, tags)

    #### python
    try:
        pycnf = cnf.get('.py')
    except PathNotExist:
        pycnf = None

    if pycnf:
        logg.info("import python config")
        flags = pycnf.getval('.flags', mklist=True, default=[]) + DEFAULT_PYTHON_FLAGS
        flags += roadrunner.mod.python.DEFAULT_FLAGS
        logg.info(f"using flags:{flags}")
        tags = flags + ['include', 'inc']
        pymods = roadrunner.mod.python.gather_python(pycnf, wd, tags, flags, raw=True)
        pyexport = export_files(wd, pymods, paths, UNIT_PYTHON_PATH)
        pathsSet = set()
        for fname in pyexport:
            locpath = str(fname.parent)
            pathsSet.add(locpath)
        pypaths = list(pathsSet)

        #script file to source at start
        scriptName = pycnf.getval(".scriptFile", path=True, default=None, assertSingle=True)
        scriptFile = rr.workdir_import(wd, scriptName, dirnames=rtd)
        if scriptFile is not None:
            logg.info(f"imported python script to {scriptFile}")

        #script content
        scriptInline = cnf.getval(".script", default=None)

        if scriptInline is not None:
            with open(td / "inline.py", "w") as fh:
                origin = cnf.get(".script").origin
                print("from rrenv import *", file=fh)
                print(f"# Script ({origin})", file=fh)
                print(scriptInline, file=fh)
            logg.info(f"imported inline script to {td / 'inline.py'}")

        if scriptFile is None and scriptInline is None:
            logg.warning(f"no python source found! .scriptFile or .script need to be set in {pycnf.getname()}")


        pypathlist = ':'.join(map(str, fn.relpath(pypaths, rtd)))
        script = scriptFile.parts[-1] if scriptFile is not None else "inline.py"
        with open(td / "py.cmd", "w") as fh:
            #print(f"#!/bin/sh", file=fh) #the racyics servers seem to have a problem with the shebang
            print(f"PYTHONPATH=$PYTHONPATH:.:{pypathlist} python3 {script}", file=fh)
        (td / "py.cmd").chmod(0o755)

        pyvars = roadrunner.mod.files.gather_env_files(pycnf, wd, tags)
        pyvars['script'] = 'py.cmd'
        for key in pyvars:
            if key in vars:
                logg.warn(f"python env overwrites env:{key}")
        vars.update(pyvars)

    #write RREnv.sv
    fname = roadrunner.mod.verilog.write_env_file(td, vars)
    files_sv.insert(0, fn.relpath(fname, wd))

    #write rrenv.py
    with open(td / roadrunner.mod.python.RRENV_FILE, "w") as fh:
        print(f"# Environment", file=fh)
        print(roadrunner.mod.python.python_val(vars), file=fh)

    #write sources makefile
    def mkmklst(lst):
        strlst = [f"    $(ICPRO_DIR)/{x} \\" for x in lst]
        return "\n".join(strlst)

    with open(td / NCSIM_SOURCES_FILE, "w") as fh:
        print(rr.template(NCSIM_SOURCES_TEMPLATE).format(
            toplevel=toplevel,
            v_files=mkmklst(files_v),
            sv_files=mkmklst(files_sv),
            vhdl_files=mkmklst(files_vhdl),
            c_files=mkmklst(files_c)
        ), file=fh)

    #write vars makefile
    defines = []
    for d in dd['define']:
        defines.append(f"IRUN_OPTS += -define {d.strip()}")

    with open(td / NCSIM_VARS_FILE, "w") as fh:
        print(rr.template(NCSIM_VARS_TEMPLATE).format(
            testcase=testcase,
            defines="\n".join(defines)
        ), file=fh)

    fn.banner(f"ICPRO - NCSim - Unit:{unit} - /Testcase:{testcase}")


def export_files(wd, lst, units, pathtempl):
    slugs = []
    fnames = []
    hist = {}
    for node in lst:
        node_path = node.resolve().getpath()
        #target dir
        m = [unit_name for unit_path,unit_name in units if issubtree(unit_path, node_path)]
        if len(m) > 1:
            logg.warning(f"multiple units defined for path|{path2name(node_path)} - " + " ".join(m))
        if len(m) == 0:
            logg.warning(f"no units defined for path|{path2name(node_path)} - using 'rr'")
            m = ['rr']
        target_dir = pathlib.Path(pathtempl.format(unit=m[0]))
        if len(m) == 0:
            logg.warning(f"cannot find a unit mapping for:{path2name(node_path)}")
        slugs.append(target_dir)
        #file
        _, node_value = node.strip(path=True)
        target_file = pathlib.Path(node_value.name)
        fnames.append(target_file)
        logg.debug(f"{path2name(node_path)} -> {target_dir / target_file}")
        #check for duplicated filenames
        key = (target_dir, target_file)
        if key in hist:
            logg.warning(f"file export path duplicated: {target_dir / target_file} - {path2name(hist[key])} - {path2name(node_path)}")
        hist[key] = node_path

    return rr.workdir_import(wd, [x.strip(path=True) for x in lst], dirnames=slugs, filenames=fnames)
