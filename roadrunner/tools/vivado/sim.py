import logging
from pathlib import Path

import roadrunner.fn as fn
import roadrunner.mod.files
import roadrunner.mod.verilog
import roadrunner.rr as rr
from roadrunner.config import ConfigNode, PathNotExist
from roadrunner.mod.tcl import tcl_val
from roadrunner.rr import Pipeline

NAME = "Vivado"

DEFAULT_SIM_DEFINES = ["SIMULATION", "VIVADO", "XILINX"]
DEFAULT_SIM_FLAGS = ["SIMULATION", "VIVADO", "XILINX"]

def cmd_sim(cnf, pipe):
    logg = logging.getLogger("vivado")
    logg.info(fn.banner("Vivado Simulation"))

    wd = rr.workdir_init(pipe.cwd())

    pipe.configDefault(NAME, 'xsim', 'xsim $@')
    pipe.configDefault(NAME, 'xvlog', 'xvlog $@')
    pipe.configDefault(NAME, 'xvhdl', 'xvhdl $@')
    pipe.configDefault(NAME, 'xsc', 'xsc $@')
    pipe.configDefault(NAME, 'xelab', 'xelab $@')

    skipSim = cnf.getval(".skip_sim", default=False)

    do_compile(cnf, pipe)

    do_dpi(cnf, pipe)

    do_elab(cnf, pipe)

    if not skipSim:
        do_sim(cnf, pipe)

    logg.info(fn.banner("/Vivado Simulation", False))

def cmd_xsim(cnf:ConfigNode, pipe:Pipeline):
    logg = logging.getLogger("vivado")
    logg.info(fn.banner("Vivado Simulation"))

    rr.workdir_init(pipe.cwd())
    do_sim(cnf, pipe)

    fn.banner("/Vivado Simulation")

def do_compile(cnf, pipe:Pipeline):
    logg = logging.getLogger("vivado")

    wd = pipe.cwd()
    flags = cnf.getval('.flags', mklist=True, default=[]) + DEFAULT_SIM_FLAGS
    logg.debug(f"compile using flags:{flags}")
    tags = flags + ['include', 'inc']
    attrs = {'path': True, 'sv': True, 'v': True, 'vhdl': True, 'define': False, 'options': False}

    lst = rr.gather(cnf, tags, attrs.keys(), flags, path=attrs, united=False)
    dd = rr.unite(lst)

    files = []

    vars = roadrunner.mod.files.gather_env_files(cnf, wd, tags)
    roadrunner.mod.verilog.write_env_file(wd, vars)

    files += [('sv', roadrunner.mod.verilog.RRENV_FILE, [], [])]

    for itm in lst:
        defs = itm['define'] + DEFAULT_SIM_DEFINES
        incs = []
        for inc in rr.workdir_import(wd, itm['path']):
            if inc.parent not in incs:
                incs.append(inc.parent)
        files += [('sv', fname, defs, incs) for fname in rr.workdir_import(wd, itm['sv'])]
        files += [('verilog', fname, defs, incs) for fname in rr.workdir_import(wd, itm['v'])]
        files += [('vhdl', fname, defs, incs) for fname in rr.workdir_import(wd, itm['vhdl'])]

    #remove duplicates
    tmp = files
    files = []
    for itm in tmp:
        if itm not in files:
            files.append(itm)

    if 'use_glbl' in dd['options']:
        fname = pipe.loadfile('Vivado', 'glbl', 'glbl.v')
        files += [('verilog', fname, [], [])]

    #xvlog
    with open(wd / "xvlog.prj", "w") as fh:
        for typ,fname,defs,incs in files:
            if typ not in ['sv', 'verilog']:
                continue
            line = f"{typ} work {fname}"
            for d in defs:
                line += f" -d {d}"
            for i in incs:
                line += f" -i {i}"
            print(line, file=fh)

    call = rr.Call(wd, 'xvlog', tool=(NAME, 'xvlog'))
    call.addArgs(['--prj', 'xvlog.prj'])
    pipe.runCall(call)

    #xvhdl
    with open(wd / "xvhdl.prj", "w") as fh:
        for typ,fname,defs,incs in files:
            if typ != 'vhdl':
                continue
            line = f"vhdl work {fname}"
            print(line, file=fh)

    call = rr.Call(wd, 'xvhdl', tool=(NAME, 'xvhdl'))
    call.addArgs(['--prj', 'xvhdl.prj'])
    pipe.runCall(call)


def do_dpi(cnf, pipe:Pipeline):
    logg = logging.getLogger("vivado")
    wd = pipe.cwd()

    flags = cnf.getval('.flags', mklist=True, default=[]) + DEFAULT_SIM_FLAGS
    tags = flags + ['include', 'inc']
    attrs = {'c': True, 'cpath': True, 'clib': False, 'clibpath': True, 'cstd':False}

    logg.debug(f"dpi using flags:{flags}")
    lst = rr.gather(cnf, tags, attrs.keys(), flags, path=attrs, united=True)

    sources = rr.workdir_import(wd, lst['c'])
    if len(sources) == 0:
        logg.debug("dpi - skip")
        return 0
    
    paths = rr.workdir_import(wd, lst['cpath'])

    libs = lst['clib']
    libpaths = rr.workdir_import(wd, lst['clibpath'])

    if len(lst['cstd']):
        std = lst['cstd'][0]
        for x in lst['cstd'][1:]:
            if x != std:
                logg.warning("multiple different C standards defined - using first found")
    else:
        std = None

    call = rr.Call(wd, "xsc", tool=(NAME, "xsc"))
    call.addArgs(sources)
    for path in paths:
        call.addArgs(['-gcc_compile_options', f'-I{path}'])
    for lib in libpaths:
        call.addArgs(['-gcc_link_options', f'-L{lib}'])
    for lib in libs:
        call.addArgs(['-gcc_link_options', f'-l{lib}'])
    if std is not None:
        call.addArgs(['-gcc_compile_options', f'-std={std}'])

    pipe.runCall(call)

    return 0


def do_elab(cnf, pipe):
    logg = logging.getLogger("vivado")

    wd = pipe.cwd()
    toplevels = cnf.getval(".toplevel", mklist=True)
    flags = cnf.getval('.flags', mklist=True, default=[]) + DEFAULT_SIM_FLAGS
    tags = flags + ['include', 'inc']

    logg.debug(f"elab using flags: {flags}")

    #dpi
    if (wd / 'xsim.dir/work/xsc/dpi.so').exists():
        dpi = ['dpi']
    else:
        dpi = []

    #libs
    dd = rr.gather(cnf, tags, ['lib', 'options'], flags)

    #options
    if 'use_glbl' in dd['options']:
        toplevels += ['glbl']

    #optimization
    optimize = cnf.getval(".optimize", default=None)

    #parameters
    params = cnf.getval(".params", mklist=True, default=[])

    #expose
    dbName = cnf.getval(".exposeDB", default=None)
    if dbName is not None:
        pipe.expose("xsim.dir", dbName)

    #cmd
    call = rr.Call(wd, "xelab", tool=(NAME, "xelab"))
    for l in dpi:
        call.addArgs(['-sv_lib', l])
    if optimize is not None:
        call.addArgs([f'-O{optimize}'])
    call.addArgs(['--relax', '--incr', '--debug', 'typical'])
    for lib in dd['lib']:
        call.addArgs(['-L', lib])
    call.addArgs(['--snapshot', 'rrun.snap'])
    for par in params:
        call.addArgs(['-generic_top', str(par)])
    call.addArgs(toplevels)

    pipe.runCall(call)

    return 0

def do_sim(cnf:ConfigNode, pipe:Pipeline):
    logg = logging.getLogger("vivado")

    wd = pipe.cwd()

    try:
        dbName = cnf.getval('.discoverDB')
        pipe.discover("xsim.dir", dbName)
    except PathNotExist:
        pass

    roadrunner.mod.files.share(cnf, pipe)

    flags = cnf.getval('.flags', mklist=True, default=[]) + DEFAULT_SIM_FLAGS
    tags = flags + ['include', 'inc']
    pths = rr.gather(cnf, tags, ['dpi_libpath'], flags, path=True)
    libpaths = rr.workdir_import(wd, pths['dpi_libpath'])

    logg.debug(f"sim using flags:{flags}")

    #files and variables
    vars = roadrunner.mod.files.gather_env_files(cnf, wd, tags)
    with open(wd / "RRVars.tcl", "w") as fh:
        for name, var in vars.items():
            print(f"set {name} {tcl_val(var)}", file=fh)

    usegui = cnf.getval('.gui', default=None)
    view = cnf.getval('.view', default=None, path=True)

    #arguments
    #with rr.command_args(cnf) as args:
    #    args.addflag('--gui')
    #    args.addflag('--nogui')
    #    args.addstr('--view')
    #if args.gui:
    #    usegui = True
    #if args.nogui:
    #    usegui = False
    #if args.view:
    #    view = (cnf.location, Path(args.view))

    view_file = rr.workdir_import(wd, view)

    call = rr.Call(wd, "xsim", tool=(NAME, "xsim"))
    if len(libpaths):
        call.setenv('LD_LIBRARY_PATH', ":".join([str(x) for x in libpaths]))
    call.addArgs(['rrun.snap'])
    if usegui:
        call.addArgs(['-gui'])
        if view:
            call.addArgs(['-view', view_file])
    else:
        call.addArgs(['--runall'])

    pipe.runCall(call)

    return 0
