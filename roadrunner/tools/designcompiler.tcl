##############################################
# Vodafone Chair Mobile Communication System
# TU Dresden
# 
# email: mattis.hasler@tu-dresden.de
# authors: Mattis Hasler
# notes:

puts "=== SYNTH.TCL ==="

source "synth_config.tcl"

# create worklib
define_design_lib WORK -path synth_work

#clock & reset
#set resets rst_i
#set combinatorial_signals {{$resets}}

# format {<port_name> <clock_name> [<cycle_time> [<aspect_ratio>]] {<{assinged_port <delay>}> <{assinged_port <delay>}>}}
#set clocks {
#    {clk_i sys_clk {2.22 0 1.11} {{}}}
#}

# set maximum cores used on multicore machine, one licence for every core is needed
set_host_options -max_cores 6

#search path
set search_path [list .]

#load libs
foreach ulib $libUncompiled {
    read_lib $ulib
}
set link_library [concat $link_library $libCompiled $libUncompiledDb]

#import verilog files
foreach sverilog_file $systemverilog_files {
    analyze -f sverilog -library WORK $sverilog_file
}

elaborate $design_name

link

check_design > reports/design_check_link.log

########################################################################
# clocks
########################################################################

set unconstrained_ports [get_ports]
foreach clock $clocks {
    puts "CLOCK"
    puts $clock
    set name [lindex $clock 1]
    set port [lindex $clock 0]
    set period  [lindex $clock 2]

    create_clock -name $name -period $period [get_ports $port]
    set unconstrained_ports [remove_from_collection $unconstrained_ports [get_ports $port]]
    set_input_delay -clock [lindex $clock 1] 0.00 [all_inputs]
}
set_dont_touch_network [get_clocks]
set_clock_transition 0.2 [get_clocks]
set_clock_uncertainty 0.2 [get_clocks]


## toplevel needs different I/O constraining the submacros
if {![info exists is_top_level]} {
    # Additionally a load value should be set on all input ports (including real
    # clocks)
    set_load 0.25 [all_inputs]

    # A capacitance is required for all output or inout ports.
    set_load 0.25  [all_outputs]
}

if {$unconstrained_ports != {}} {
    echo "The following Signals are not constrained in the design:"
    query_objects $unconstrained_ports
}

#set_operating_conditions -library "tcbn65lptc" "NCCOM"

foreach clock $clocks {
    set_ideal_network [get_ports [lindex $clock 0]]
}

set_fix_hold [all_clocks]


compile -map_effort high -boundary_optimization
#compile_ultra 

set_dft_insertion_configuration -preserve_design_name true
set_dft_insertion_configuration -synthesis_optimization none
set_scan_configuration -clock_mixing no_mix
create_test_protocol
dft_drc > reports/pre.dft_drc

check_design > reports/design_check_compile.log
report_area -hierarchy  -nosplit > reports/area.rpt
report_timing -nworst 10 -cap  -tran -nets -nosplit > reports/timing.rpt
report_power -analysis_effort high -hier -nosplit > reports/power.rpt
report_constraints -verbose -all_violators > reports/constraint_violations.rpt
report_clock -nosplit > reports/clock.rpt

write -hierarchy -f ddc -output netlist/$design_name.ddc

#define_name_rules verilog -add_dummy_nets_in_verilog_out "SYNOPSYS_UNCONNECTED_%d"
define_name_rules verilog -add_dummy_nets
change_names -rules verilog -hierarchy

write -hierarchy -f verilog -output netlist/$design_name.v

# write the constraint file (needed for place&route)
write_sdc netlist/$design_name.sdc
write_sdf netlist/$design_name.sdf

quit
