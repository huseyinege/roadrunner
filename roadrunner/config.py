##############################################
# Vodafone Chair Mobile Communication System
# TU Dresden
# 
# email: mattis.hasler@tu-dresden.de
# authors: Mattis Hasler
# notes:

from abc import abstractmethod
import pathlib
import re
import logging

import yaml

import roadrunner.lua
import roadrunner.fn as fn

#create a config node from raw data
def makeConfigVal(raw, origin=None, location=None):
    #use given location
    if location is None:
        loc = None
    elif isinstance(location, Location):
        loc = location
    else:
        loc = Location(location)
    #load location
    if isinstance(raw, dict):
        stat = bool(raw['_static']) if '_static' in raw else False
        if '_location' in raw:
            loc = Location(raw['_location'], static=stat)
        elif '_static' in raw:
            loc = Location(raw['_static'], static=stat)
        elif stat:
            loc = Location(location, static=stat)
    #create node
    if isinstance(raw, (ConfigDict, ConfigList, ConfigLeaf)):
        cnf = raw
    if isinstance(raw, dict):
        dd = ConfigDict()
        oris = raw['__origins__'] if '__origins__' in raw else {} 
        for key, val in raw.items():
            if key == '__origins__':
                continue
            ori = oris[key] if key in oris else None
            ckey = key if key != None else '~'
            dd.setchild(ckey, makeConfigVal(val, origin=ori, location=loc))
        cnf = dd
    elif isinstance(raw, list):
        if len(raw) > 0 and isinstance(raw[-1], list) and isinstance(raw[-1][0], Origin):
            oris = raw[-1]
            raw = raw[:-1]
        else:
            oris = [None] * len(raw) 
        lst = ConfigList()
        for itm,ori in zip(raw, oris):
            lst.setchild('#', makeConfigVal(itm, origin=ori, location=loc))
        cnf = lst
    elif isinstance(raw, str) and len(raw) > 0 and raw[0] in ['=', '+', '$']:
        cnf = ConfigLink(raw)
    else:
        cnf = ConfigLeaf(raw)
    if origin:
        cnf.origin = origin
    if loc:
        cnf.location = loc
    return cnf

#dump a config line
def putConfigLine(indent, pre, post, msg):
    line = '  ' * indent + pre
    if msg:
        msglines = str(msg).splitlines()
        line += msglines[0]
        if len(msglines) > 1:
            line += '...'
    return fn.lrline(line, f"({post})" if post else None)

class NoValue(object):
    pass

class ConfigNode:
    def __init__(self):
        self.parent = None
        self.location = None
        self.origin = None

    def getId(self):
        return id(self) #TODO this seems to be unsave

    def root(self):
        curr = self
        while curr.parent is not None:
            curr = curr.parent
        return curr

    def getval(self, name, path=False, default=NoValue, mklist=False, assertSingle=False):
        try:
            cnf = self.get(name)
            raw = cnf.strip(path=path)
            if mklist and not isinstance(raw, list):
                raw = [raw]
            if assertSingle and isinstance(raw, list):
                raise ConfigError("node value is not a single value")
            return raw
        except PathNotExist:
            if default is NoValue:
                raise
            else:
                return default

    def get(self, name):
        try:
            path = name2path(name)
        except InvalidName as ex:
            ex.addtrace(ErrorTrace(self, None, 'GET'))
            raise
        return self.get_path(path)

    def setval(self, name, value, create=False):
        leaf = makeConfigVal(value)
        self.set(name, leaf, create=create)

    def set(self, name, raw, create=False):
        self.set_path(name2path(name), raw, create=create)

    def get_path(self, path):
        try:
            val = self._get_path(path)
        except PathNotExist as ex:
            ex.addtrace(ErrorTrace(self, path, "GET"))
            raise
        except ConfigError as ex:
            ex.addtrace(ErrorTrace(self, path, "GET"))
            raise
        return val

    def _get_path(self, path):
        if len(path) > 0:
            #root hook
            if path[0] == ':':
                return self.root()._get_path(path[1:])
            #one level up
            if path[0] == '':
                if self.parent is None:
                    raise InvalidPath("reached root, cannot go up")
                return self.parent._get_path(path[1:])
        #this node
        if len(path) == 0:
            return self
        #current part
        part = path[0]
        #get attribute
        try:
            child = self.getchild(part)
        except ChildError:
            raise PathNotExist("path does not exist")
        #go down
        return child._get_path(path[1:])

    def set_path(self, path, raw, create=False, relpath=False):
        try:
            self._set_path(path, raw, create=create, relpath=relpath)
        except ConfigError as ex:
            ex.addtrace(ErrorTrace(self, path, 'SET'))
            raise 

    def _set_path(self, path, raw, create=False, relpath=False):
        if path == []:
            raise InvalidPath(f"cannot set to empty path @({self.getname()})")
        if not isinstance(raw, ConfigNode):
            raise BadValue(f'need a ConfigValue to set')
        #go down 
        curr = self
        key = path[0]
        for part in path[1:]:
            try:
                curr = curr._get_path([key])
            except PathNotExist:
                if not create:
                    raise
                if part == '#':
                    itm = makeConfigVal([])
                else:
                    itm = makeConfigVal({})
                curr.setchild(key, itm)
                curr = itm
            key = part
        curr.setchild(key, raw)

    def getchild(self, key):
        if self.parent == self: #trick linter
            return self
        raise ChildError(f"getting child unsupported ({key}) ({self.getname()})")

    def findchild(self, child):
        raise BadValue(f"finding child unsupported ({self.getname()})")

    def setchild(self, key, child):
        raise BadValue(f"setting child unsupported")

    def getname(self):
        return path2name(self.getpath())
    
    def getpath(self):
        if self.parent is None:
            return [':']
        try:
            key = self.parent.findchild(self)
        except BadValue:
            raise TreeCorrupt(f"cannot build name - not registered @ parent")
        return self.parent.getpath() + [key]

    @abstractmethod
    def strip(self, path=False):
        raise BadValue(f"stripping unsupported")

    def resolve(self):
        return self #in most config Classes it's just the identity
    
    def vardir(self):
        return makeVarDir(self)

class ConfigLeaf(ConfigNode):
    def __init__(self, value):
        super().__init__()
        self.value = value

    def __repr__(self):
        return f"ConfigLeaf({self.value})"

    def dump(self, indent=0, pre='', _=None):
        return putConfigLine(indent, pre, self.origin, str(self.value))

    def follow(self):
        if not isinstance(self.value, str) or len(self.value) == 0 or self.value[0] != '=':
            return self
        path = None
        try:
            path = name2path(self.value[1:])
            return self.parent._get_path(path)
        except InvalidName as ex:
            ex.addtrace(ErrorTrace(self, None, "FOLLOW"))
            raise
        except ConfigError as ex:
            ex.addtrace(ErrorTrace(self, path, "FOLLOW"))
            raise
    
    def strip(self, path=False):
        if isinstance(self.value, str):
            fns = getnatives(self)
            processed = roadrunner.lua.inline(self.value, self, fns)
        else:
            processed = self.value
        #path
        if path:
            return (self.location, pathlib.Path(processed))
        else:
            return processed

    def clone(self, parent=None):
        return ConfigLeaf(self.value, parent)

class ConfigList(ConfigNode, list):
    def getchild(self, key):
        if not isinstance(key, int):
            raise BadValue(f"ConfigList must take int as child key, got:{key} type:{type(key)}")
        try:
            return self[key]
        except IndexError:
            raise ChildError(f"child not found by index:{key}")
    
    def findchild(self, child):
        for idx,raw in enumerate(self):
            try:
                link = raw.resolve()
            except ConfigError:
                link = None
            if link == child or raw == child: #follow links or not
                return idx
        raise ChildError(f'cannot find index of child:{child}')

    def setchild(self, key, child):
        if not isinstance(child, ConfigNode):
            raise BadValue(f'can only set ConfigNodes as Values not {type(child)}')
        if key == '#':
            self.append(child)
            child.parent = self
            return
        if not isinstance(key, int):
            raise BadValue(f'can only set integer keys in list')
        if key in self:
            self[key].parent = None
        self[key] = child
        child.parent = self

    def dump(self, indent=0, pre='', followed=None):
        msg = putConfigLine(indent, pre, self.origin, '')
        for val in self:
            msg += "\n" + val.dump(indent+1, '- ', followed)
        return msg

    def clone(self, parent=None):
        lst = ConfigList([], parent)
        for raw in self:
            lst.append(raw.clone())

    def strip(self, path=False):
        return [raw.strip(path=path) for raw in self]

    def merge(self, lst):
        for itm in lst:
            itm.parent = self
            self.append(itm)
    
class ConfigDict(ConfigNode, dict):
    def dump(self, indent=0, pre='config:', followed:set=None):
        msg = putConfigLine(indent, pre, self.origin, '')
        for key,val in self.items():
            msg += "\n" + val.dump(indent+1, key + ": ", followed)
        return msg

    def getchild(self, key):
        if not isinstance(key, str):
            raise BadValue(f"ConfigDict must take a string as child key, got:{key} type:{type(key)}")
        try:
            return self[key]
        except KeyError:
            raise ChildError(f"child not found by key:{key}")

    def findchild(self, child):
        for key,raw in self.items():
            try:
                link = raw.resolve()
            except ConfigError:
                link = None
            if link == child or raw == child: #follow links or not
                return key
        raise ChildError(f'cannot find key of child:{child}')

    def setchild(self, key, child):
        if not isinstance(key, str):
            raise BadValue(f'can only set string keys in ConfigDict')
        if not isinstance(child, ConfigNode):
            raise BadValue(f'can only set ConfigNodes as Values not {type(child)}')
        if key in self:
            self[key].parent = None
        self[key] = child
        child.parent = self

    def merge(self, dd, overwrite=True):
        for key,val in dd.items():
            try:
                curr = self[key]
                if isinstance(curr, ConfigDict) and isinstance(val, ConfigDict):
                    curr.merge(val, overwrite=overwrite)
                elif isinstance(curr, ConfigList) and isinstance(val, ConfigList):
                    curr.merge(val)
                elif overwrite:
                    val.parent = self
                    self[key] = val
            except KeyError:
                val.parent = self
                self[key] = val

    #make a deep copy
    def clone(self, parent=None):
        res = ConfigDict({}, None)
        for key,val in self.items():
            res[key] = val.clone(parent=res)
        return res

    def strip(self, path=False):
        return {key: val.strip(path=path) for key,val in self.items()}

class ConfigLink(ConfigNode):
    def __init__(self, target):
        super().__init__()
        self.target = target    #name to the linked ConfigNode
        self.link = None        #actuall ConfigNode to link to

    def __repr__(self):
        return f"ConfigLink({self.target})"

    def dump(self, indent=0, pre='', followed:set=None):
        if followed is None:
            followed = set()
        if self.link: #follow link only if already visited
            if self in followed:
                return putConfigLine(indent, pre, "", f"{self.target} rec")
            else:
                followed.add(self)
            pre += f"{self.target}: "
            return self.link.dump(indent, pre, followed)
        else:
            msg = f"{self.target} (cold)"
            return putConfigLine(indent, pre, self.origin, msg)

    def _follow(self):
        path = None
        try:
            path = name2path(self.target[1:])
            self.link = self._get_path([''] + path)
        except InvalidName as ex:
            ex.addtrace(ErrorTrace(self, None, "FOLLOW"))
            raise
        except PathNotExist:
            raise LinkNotExist(f"dead link", ErrorTrace(self, path, "FOLLOW"))
        except ConfigError as ex:
            ex.addtrace(ErrorTrace(self, path, "FOLLOW"))
            raise

    def resolve(self):
        if self.link:
            pass
        elif self.target[0] == '=': #this is a symlink
            self._follow()
        elif self.target[0] == '+': #this is a load
            self._load()
        elif self.target[0] == '$': #this is a script
            self._run()
        else:
            raise LinkError(f"cannot parse target:{self.target}")
        return self.link

    def _load(self):
        fname = pathlib.Path(self.target[1:]) / 'RR'
        try:
            dd = fromfile(self.location / fname)
        except FileNotFoundError:
            raise LinkNotExist(f"File not found:{fname}", ErrorTrace(self, None, "LOADFILE"))
        dd.parent = self.parent #TODO this is dangerous
        self.link = dd
        #logging.getLogger('cnf').info(f"loading YAML:{fname} to ({self.getname()})") #disabled cause of endless recursion

    def _run(self):
        fns = getnatives(self)
        if self.target[1] == '$':
            raw = roadrunner.lua.exec(self.target[2:], self, fns, eval=False) #script
        else:
            raw = roadrunner.lua.exec(self.target[1:], self, fns, eval=True) #expression
        node = makeConfigVal(raw)
        node.parent = self.parent
        self.link = node

    def getchild(self, key):
        self.resolve()
        return self.link.getchild(key)

    def findchild(self, child):
        self.resolve()
        return self.link.findchild(child)
    
    def setchild(self, key, child):
        raise LinkError("cannot set child through a Link")

    def strip(self, path=False):
        self.resolve()
        return self.link.strip(path)

def makeVarDir(cnf):
    raw = {}

    def iterparents(node):
        node = node.parent
        while node != None:
            yield node
            node = node.parent

    #find all variable definitions       
    for ctxt in iterparents(cnf):
        try:
            if not isinstance(ctxt, ConfigDict):
                continue
            for key,node in ctxt.get('.vars').items():
                if key not in raw:
                    raw[key] = node
        except PathNotExist:
            continue

    return raw

def getnatives(cnf):
    return cnf.getval(":_setup.natives", default={})

class SafeLineLoader(yaml.loader.SafeLoader):
    def construct_mapping(self, node, deep=False):
        mapping = super(SafeLineLoader, self).construct_mapping(node, deep)
        lines = {}
        for key_node,val_node in node.value:
            key = self.construct_object(key_node, deep=deep)
            if key not in mapping:
                raise Exception("cannot create mapping")
            lines[key] = Origin(key_node.start_mark.line+1, key_node.start_mark.column, key_node.start_mark.name)
        mapping['__origins__'] = lines
        return mapping

    def construct_sequence(self, node):
        lst = super(SafeLineLoader, self).construct_sequence(node)
        lines = []
        for itm_node in node.value:
            lines.append(Origin(itm_node.start_mark.line+1, itm_node.start_mark.column, itm_node.start_mark.name))
        lst.append(lines)
        return lst

class Location():
    def __init__(self, path, static=False):
        if isinstance(path, str):
            self.path = pathlib.Path(path)
        elif isinstance(path, pathlib.Path):
            self.path = path
        elif isinstance(path, Location):
            self.path = path.path
        else:
            self.path = None
        assert path is not None
        self.static = static

    def __str__(self):
        return str(self.path)

    def __repr__(self):
        return f"Loc{'s' if self.static else ''}('{str(self.path)}')"

    def __truediv__(self, other):
        return self.path / other

    def __eq__(self, other: object) -> bool:
        return self.path == other.path and self.static == other.static

class Origin():
    def __init__(self, line, column, f=None):
        self.line = line
        self.column = column
        self.file = f

    def __repr__(self):
        return f"Ori({self.file}:{self.line})"

    def __str__(self):
        return f"{self.file}:{self.line}"

def fromfile(fname):
    if not isinstance(fname, pathlib.Path):
        raise BadValue("filename must be a pathlib.Path")
    with open(fname, "r") as fh:
        f_json = yaml.load(fh, Loader=SafeLineLoader)
    orig = Origin(0, 0, fname)
    return makeConfigVal(f_json, orig, fname.parent)

def fromstr(raw:str, location:pathlib.Path):
    f_json = yaml.load(raw, Loader=SafeLineLoader)
    return makeConfigVal(f_json, location=location)

def tofile(cnf, fname):
    with open(fname, "w") as fh:
        fh.write(tostr(cnf))

def tostr(cnf) -> str:
    def descend(cnf):
        ret = {}
        for key,val in cnf.items():
            if isinstance(val, ConfigDict):
                ret[key] = descend(val)
            elif isinstance(val, list):
                ret[key] = [str(x) for x in val]
            else:
                ret[key] = val.strip()
        return ret
    dd = descend(cnf)
    return yaml.dump(dd)

def name2path(name):
    if name == "":
        raise InvalidName("name cannot be empty")
    if name[0] not in  ['.', ':']:
        raise InvalidName(f"path must begin with . or : ({name})")
    path = []
    pos = None
    def mkpart(src):
        try:
            return int(src)
        except ValueError:
            return src
    for m in re.finditer(r'[:\.]', name):
        if pos is not None:
            part = name[pos:m.start()]
            path.append(mkpart(part))
        pos = m.start() + 1
        if m[0] == ':':
            path.append(':')
    if pos < len(name):
        path.append(mkpart(name[pos:]))
    return path

def path2name(path):
    name = ""
    skipdot = False
    for itm in path:
        if not isinstance(itm, (int, str)):
            raise InvalidPath(f'a path may only contain int and str')
        isroot = True if itm == ':' else False
        if not skipdot and not isroot:
            name += '.'
        skipdot = isroot
        name += str(itm)
    if path[-1] == '':
        name += '.'
    return name

#checks if the path given in item is located below the base path
def issubtree(base, item):
    return base == item[:len(base)]

class ErrorTrace(object):
    def __init__(self, root, path, op):
        self.root = root
        self.path = path
        self.op = op

    def __str__(self):
        rootname = "NA" if self.root is None else self.root.getname()
        opname = "NOOP" if self.op is None else self.op
        pathname = "NA" if self.path is None else path2name(self.path)
        return f"  ({rootname}) {opname} ({pathname})"

    def traceline(self):
        origin = self.root.origin
        return fn.lrline(f'  {self}', f'({origin})' if origin else None)

class ConfigError(Exception):
    REASON = "Error"
    DESCRIPTION = None
    def __init__(self, msg, crumb=None):
        self.msg = msg
        self.trace = []
        if crumb:
            if isinstance(crumb, list):
                self.trace.extend(crumb)
            else:
                self.trace.append(crumb)

    def __str__(self):
        msg = ""
        if len(self.trace) == 0:
            msg += "No tracing info\n"
        else:
            msg += "Trace:\n"
        for crumb in reversed(self.trace):
            msg += crumb.traceline() + '\n'
        msg += f"  {self.REASON}: {self.msg}"
        return msg

    def addtrace(self, crumb):
        self.trace.append(crumb)

class InvalidPath(ConfigError):
    REASON = "InvalidPath"
    DESCRIPTION = "The path at hand contains invalid parts (not int or str) or points to an invalid loaction like above the root node"

class InvalidName(ConfigError):
    REASON = "InvalidName"
    DESCRIPTION = "The string at hand is not a correctly formated path"

class PathNotExist(ConfigError):
    REASON = "PathNotExist"
    DESCRIPTION = "Some part of the path points to an non-existing child"

class LinkNotExist(ConfigError):
    REASON = "LinkNotExist"
    DESCRIPTION = "The target of the link does not exist, e.g. the config node or the file to be loaded"

class BadValue(ConfigError):
    REASON = "BadValue"
    DESCRIPTION = "A bad value has been used, like a str to access a list"

class TreeCorrupt(ConfigError):
    REASON = "TreeCorrupt"
    DESCRIPTION = "Something in the config tree is wrong that should not be"

class ChildError(ConfigError):
    REASON = "ChildError"
    DESCRIPTION = "Problem with child access, like accessing child in leaf node or indexing list beyond its size"

class LinkError(ConfigError):
    REASON = "LinkError"
    DESCRIPTION = "Bad usage of a link like trying to set the child of a Link"
