def _tee(inStream, outName:str, toShell:bool=True, regex:str=None, matches:list=None):
    try:
        with open(outName, 'w') as outStream:
            for raw in iter(inStream.readline, b''):
                line = raw.decode(sys.stdout.encoding)
                if regex:
                    m = re.match(regex, line)
                    if m:
                        matches.append(m)
                if toShell:
                    print(line, end='')
                print(line, end='', file=outStream, flush=True)
    except ValueError:
        pass
    #print("TEE file closed")

class CommandGroupRoot:
    def unregsiter(self, cmd:CommandGroup|Command):
        pass
    def register(self, cmd:CommandGroup|Command):
        pass

class CommandGroupTearing(Exception):
    pass

class CommandGroup:
    root = None

    @classmethod
    def rootGroup(cls):
        if cls.root is None:
            cls.root = CommandGroup(CommandGroupRoot())
        return cls.root

    def __init__(self, parent:CommandGroup=None):
        self.parent = parent if parent is not None else self.rootGroup()
        self.cmdSet = set()
        self.cmdCond = threading.Condition()
        self.cmdHist = []
        self.intCounter = 0

    def activate(self):
        self.parent.register(self)

    def deactivate(self):
        self.parent.unregister(self)

    def __enter__(self) -> CommandGroup:
        self.activate()
        return self

    def __exit__(self, _, __, ___):
        self.waitEmpty()
        self.deactivate()

    def __len__(self) -> int:
        return len(self.cmdSet)

    def isEmpty(self) -> bool:
        return len(self.cmdSet) == 0

    def waitEmpty(self, timeout:float=None):
        with self.cmdCond:
            self.cmdCond.wait_for(self.isEmpty, timeout=timeout)

    def register(self, cmd:Command):
        if self.intCounter:
            raise CommandGroupTearing()
        with self.cmdCond:
            self.cmdHist.append(cmd)
            self.cmdSet.add(cmd)
            self.cmdCond.notify_all()

    def unregister(self, cmd:Command):
        with self.cmdCond:
            self.cmdSet.remove(cmd)
            self.cmdCond.notify_all()

    def interrupt(self):
        self.intCounter += 1
        for cmd in self.cmdSet:
            cmd.interrupt() #works on both groups and Commands


class Command:
    cmd_local = threading.local()

    def __init__(self, cmdnode:ConfigNode, cmdgroup:CommandGroup, caller=None):
        self.cnf = cmdnode
        self.group = cmdgroup if cmdgroup is not None else CommandGroup.rootGroup()
        self.caller = caller.cnf if caller is not None else None
        self.thread = threading.Thread(target=self.run, name=f"Command({self.cnf.getname()})")
        self.int_cond = threading.Condition()
        self.int_counter = 0
        self.group.register(self)
        self.retvalCond = threading.Condition()
        self.retval = None

    def start(self):
        self.thread.start()

    def run(self):
        try:
            self.cmd_local.command = self
            #set the link 
            if self.caller:
                self.cnf.setval(".link", f"={self.caller.getname()}")
            #call parallel
            #try:
            #    paras = self.cnf.get(".parallel")
            #    fn.deprecated(".parallel attributes are deprecated - use the Runner tool instead")
            #    if not isinstance(paras, ConfigList):
            #        paras = [paras]
            #    for para in paras:
            #        cmd = Command(para.resolve(), self)
            #        cmd.start()
            #except PathNotExist:
            #    pass
            #get tool
            toolDesc = self.cnf.getval(".tool").split('.')
            toolName, toolFn = (toolDesc[0], 'run') if len(toolDesc) < 2 else toolDesc
            toolfn = tools.gettool(toolName, toolFn)
            #run
            ret = toolfn(self.cnf)
            if ret is None:
                logging.getLogger("RR").warn(f"tool:{toolDesc} returned None - change to 0")
                ret = 0
            #
            with self.retvalCond:
                self.retval = ret
                self.retvalCond.notify_all()
        finally:
            self.group.unregister(self)

    def interrupt(self):
        print("got interrupt", self.cnf.getname())
        with self.int_cond:
            self.int_counter += 1
            self.int_cond.notify()

    def is_interrupted(self):
        return self.int_counter > 0

    @classmethod
    def wait_interrupt(self, timeout=None):
        cmd = self.cmd_local.command
        with cmd.int_cond:
            ret = cmd.int_cond.wait_for(cmd.is_interrupted, timeout=timeout)
            if ret:
                cmd.int_counter -= 1
            return ret

    def wait_retval(self):
        with self.retvalCond:
            self.retvalCond.wait_for(lambda: self.retval is not None)

class Proc(object):
    def __init__(
        self, args, wd:pathlib.Path, name:str, toShell:bool=True,
        interactive:bool=False, stdoutMatch:str=None, environ:dict=None
    ):
        self.killSequence = [
            (None, None), #run forever
            (signal.SIGINT, 0.5),
            (signal.SIGINT, 5.0),   #double sigint
            (signal.SIGTERM, 5.0),
            (signal.SIGKILL, 5.0)
        ]
        #cmd
        cmd = args
        #stdout scanner
        self.matches = []
        #start process
        self.interactive = interactive
        if interactive:
            #save terminal state
            fn = sys.stdin.fileno()
            self.termattr = termios.tcgetattr(fn)
            #start process
            self.popen = subprocess.Popen(cmd, cwd=wd)
        else:
            if environ is not None:
                env = os.environ.copy()
                env.update(environ)
            else:
                env = None
            self.popen = subprocess.Popen(
                cmd, cwd=wd, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                start_new_session=True, env=env
            )
            #TEE
            fStdout = wd / ('stdout' if name is None else f'{name}.stdout')
            fStderr = wd / ('stderr' if name is None else f'{name}.stderr')
            args = (self.popen.stdout, fStdout, toShell, stdoutMatch, self.matches)
            self.threadStdout = threading.Thread(
                target=_tee, args=args, daemon=True, name=f"TeeStdOut({wd})"
            )
            self.threadStdout.start()
            args = (self.popen.stderr, fStderr, toShell)
            self.threadStderr = threading.Thread(
                target=_tee, args=args, daemon=True, name=f"TeeStdErr({wd})"
            )
            self.threadStderr.start()
        #
        self.process = [psutil.Process(self.popen.pid)]
        self.log = logging.getLogger('rr')


    def __del__(self):
        if self.popen.returncode is None:
            self.log.error("SubProcess was not terminated - please investigate")
        #restore term
        if self.interactive:
            termios.tcsetattr(sys.stdin.fileno(), termios.TCSADRAIN, self.termattr)

    def _update_process(self):
        for proc in self.process:
            try:
                for child in proc.children(recursive=True):
                    if child not in self.process:
                        self.process.append(child)
                        self.log.debug(f"new child process: {child.pid}")
            except psutil.NoSuchProcess:
                pass
        alive = []
        for proc in self.process:
            if proc.is_running():
                alive.append(proc)
            else:
                self.log.debug(f"child disappeared: {proc.pid}")
        self.process = alive

    def signal(self, sig):
        self._update_process()
        for p in self.process:
            try:
                p.send_signal(sig)
            except psutil.NoSuchProcess:
                pass

    def finish(self):
        killIter = iter(self.killSequence)
        killStep = True
        killTime = None
        while True:
            if killTime and killTime < time.time():
                killStep = True
            if killStep:
                try:
                    sig, delay = next(killIter)
                except StopIteration:
                    self.log.warn("kill sequence end")
                    break
                if sig:
                    self.log.info(f"signal: {sig.name} --> {delay}")
                    self.signal(sig)
                killTime = time.time() + delay if delay else None
                killStep = False
            self._update_process()
            if self.popen.returncode is None and self.popen.poll() is not None:
                self.log.debug("main subprocess exited")
                #if killTime is None: #TODO: why start kill sequence when main proc is gone
                #    killStep = True
            if len(self.process) == 0:
                self.log.debug("all childs gone - break")
                break
            delay = max(0, 0 if killTime is None else killTime - time.time())
            if Command.wait_interrupt(timeout=delay):
                self.log.debug("got interrupt - killStep!")
                killStep = True
        self.popen.poll()
        #restore term
        if self.interactive:
            termios.tcsetattr(sys.stdin.fileno(), termios.TCSADRAIN, self.termattr)
        return self.popen.returncode

def proc_run(args, wd, name, toShell=True, env={}):
    cfile = f"{name}.cmd"
    fn.command_file(wd / cfile, args, env)
    return proc_script(cfile, wd, name, toShell, env)

def proc_script(script, wd, name, toShell=True, env={}):
    p = Proc(['/bin/sh', "./" + script], wd, name, toShell)
    return p.finish()

def proc_become(args, wd, name):
    fn.banner("/RoadRunnner", f"switch to process: {name}")
    os.chdir(wd)
    os.execvp(args[0], args)

def tool_getattr(cnf, tool, attr, version=None, path=False, default=NoValue, mklist=False):
    if version is None:
        version = '_'
    return cnf.getval(f":_setup.{tool}.{version}.{attr}", path, default, mklist)

def tool_loadcmd(cnf, wd, tool, cmd, version=None):
    script = tool_getattr(cnf, tool, cmd, version)
    fname = wd / f"{cmd}.tool"
    with open(fname, "w") as fh:
        #print("#!/bin/sh", file=fh)
        print(script, file=fh)
    fname.chmod(0o755)
    return [f"./{cmd}.tool"]

def process_status():
    def procStatusThread():
        start = time.time()
        stat = fn.status(0, 0, 1, fmt="t:{:.1f} c:{} p:{}")
        proc_me = psutil.Process(os.getpid())
        while True:
            #time
            running = time.time() - start
            #procs
            nprocs = len(proc_me.children(recursive=True))
            #commands
            ncmd = len(CommandGroup.rootGroup())
            #
            stat.update(running, ncmd, nprocs)
    th = threading.Thread(target=procStatusThread, daemon=True, name="ProcStatus")
    th.start()

