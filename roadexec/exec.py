from __future__ import annotations
from dataclasses import dataclass
from pathlib import Path
import logging
from urllib.request import DataHandler
from roadexec.fn import etype, banner, relpath
import roadexec.proc as proc
import threading
from roadexec.fn import Config, clone
import roadexec.shares

@dataclass
class CallItem:
    name: str
    script: Path
    abortOnError: bool
    interactive: bool

@dataclass
class LinkItem:
    name: str       #name of a share
    target: Path

@dataclass
class ResultItem:
    work: Path
    result: Path

class Exec:
    def __init__(self, group:Group, name:str):
        group._addSub(self, name)
        self.grp = (group, name)
        self.log = logging.getLogger("exec")
        self.calls = []
        self.links = []
        self.results = []
        self.proc = None
        self.thread = threading.Thread(target=self.run,
            name=f"Exec({self.getPos()})")
        self.threadRetVal = {}
        self.threadFinish = threading.Event()
        self.interrupted = False
        self.resultDir = None

    def getWd(self):
        return self.grp[0].getWd() / self.grp[1] 
        
    def getPath(self):
        return self.grp[0].getPath() + [self.grp[1]]

    def getPos(self):
        return ".".join(self.getPath())

    #create a tool invokation script an return its position
    def loadtool(self, config:Config, tool, cmd, version=None):
        script = config.getattr(tool, cmd, version)
        dir = self.getWd() / "tools"
        dir.mkdir(exist_ok=True)
        fname = dir / f"{tool}.{cmd}.sh"
        with open(fname, "w") as fh:
            #print("#!/bin/sh", file=fh)
            print(script, file=fh)
        fname.chmod(0o755)

    #link a file defined by a tool
    def loadfile(self, config:Config, name, tool, att, version=None):
        src = Path(config.getattr(tool, att, version))
        dir = self.getWd() / "tools"
        dir.mkdir(exist_ok=True)
        dest = dir / name
        if dest.is_symlink(): #remove link from earlier
            dest.unlink()
        dest.symlink_to(src)

    #run a script file
    def addCall(self, name:str, script:Path, abortOnError:bool, interactive:bool):
        self.calls.append(CallItem(
            name=name,
            script=script,
            abortOnError=abortOnError,
            interactive=interactive
        ))

    def _call(self, name:str, script:Path, interactive:bool):
        slug = name if str(self.getWd()) == '.' else f"{self.getWd()} - {name}"
        self.log.info(banner(slug))
        if interactive:
            self.log.info("Start Interactive Console")
        p = proc.Proc(["/bin/sh", script], self.getWd(), name, interactive=interactive)
        self.proc = p
        ret = p.finish()
        self.proc = None
        self.log.info(banner('/' + slug, False))
        return ret

    def run(self):
        abort = False
        self.log.debug("exec run start")
        #create Links
        for item in self.links:
            dest = self.getWd() / item.target
            if dest.is_symlink(): #remove link from earlier
                dest.unlink()
            elif dest.exists():
                self.log.warning(f"share:{item.name} file:{dest} is not a symlink, cannot replace")
                continue
            try:
                share = self.grp[0].shares.discover(item.name)
            except roadexec.shares.NotExposed:
                self.log.warning(f"share Link:{item.name} has not been exposed")
                abort = True
                break
            source = relpath(share, self.getWd())
            dest.symlink_to(source)
        #calls
        retAccu = 0
        if not abort:
            for call in self.calls:
                ret = self._call(call.name, call.script, call.interactive)
                self.threadRetVal[call.name] = ret
                retAccu |= ret
                if (call.abortOnError and retAccu != 0) or self.interrupted:
                    abort = True
                    self.log.warning(f"call:{call.name} returned:{ret} - aborting")
                    break
        #exports results
        if not abort:
            for item in self.results:
                self.log.info(f"export state {item.work} -> {item.result}")
                dest = self.resultDir / item.result
                if item.work.exists():
                    clone(item.work, dest)
                else:
                    self.log.warn(f"skip export state:{item.work} - does not exist")
        #
        self.log.debug("exec run end")
        self.threadFinish.set()

    def expose(self, file:Path, name:str=None):
        fullpath = self.getWd() / file
        if name is None:
            name = str(file)
        self.grp[0].shares.expose(fullpath, name)

    def discover(self, file:Path, name:str=None):
        if name is None:
            name = str(file)
        self.links.append(LinkItem(name=name, target=file))
    
    def interrupt(self):
        self.interrupted = True
        p = self.proc
        if p is not None:
            p.interrupt()

    def result(self, config:Config, name:str, rr:str):
        #todo load results paths
        root = Path(config.get('resultDir')) #TODO load result path from file
        root.mkdir(exist_ok=True)
        results = root / name
        results.mkdir(exist_ok=True)
        #todo clean result dir and backup results
        self.resultDir = results
        if rr:
            with open(results / "RR", "w") as fh:
                print(rr, file=fh)

    def export(self, work:Path, result:Path=None):
        if result is None:
            result = work
        self.results.append(ResultItem(work, result))


class Group:
    DEFAULT_SHARE_SCOPE = {'sequence': False, 'parallel': False, 'pool': True}

    def findPos(self, pos:str) -> Group:
        path = pos.split('.')
        group = self._findPath(path[:-1])
        return group, path[-1]

    def _findPath(self, path:list[str]):
        if path == []:
            return self
        else:
            try:
                return self.sub[path[0]]._findPath(path[1:])
            except KeyError:
                raise Exception("parent group does not exist")

    def __init__(self, parent:Group, name:str=None, mode:str="sequence", shareScope:bool=None, config:Config=None):
        if parent is None:
            self.parent = None
            self.name = None
        else:
            etype((name, str))
            self.parent = parent
            self.parent._addSub(self, name)
            self.name = name
        self.mode = mode
        self.sub = {}
        if shareScope is None:
            shareScope = self.DEFAULT_SHARE_SCOPE[self.mode]
        if self.parent is None:
            etype((config, Config))
            self.shares = roadexec.shares.load(config)
        elif shareScope:
            self.shares = roadexec.shares.ShareMap()
        else:
            self.shares = self.parent.shares
        self.log = logging.getLogger('group')
        self.thread = threading.Thread(target=self.run,
            name=f"Group({self.getPos()})")
        self.threadRetVal = None
        self.threadFinish = threading.Event()
        self.interrupted = False

    def getPath(self):
        if self.parent is None:
            return []
        else:
            return self.parent.getPath() + [self.name]

    def getPos(self):
        return ".".join(self.getPath())

    def getWd(self):
        if self.parent is None:
            return Path('.')
        else:
            return self.parent.getWd() / self.name

    def _addSub(self, sub:Group|Exec, name:str):
        etype((sub, (Group,Exec)), (name, str))
        if name in self.sub:
            raise Exception("subgroup already exists")
        self.sub[name] = sub

    def run(self):
        self.log.debug(f"group run start {self.getPos()}")
        items = list(self.sub.values())
        if self.mode == 'sequence':
            for sub in self.sub.values():
                sub.thread.start()
                sub.threadFinish.wait()
                if self.interrupted:
                    break
        elif self.mode == 'parallel':
            for sub in self.sub.values():
                sub.thread.start()
            for sub in self.sub.values():
                sub.threadFinish.wait()
        else:
            raise Exception(f"unknown group mode:{self.mode}")
        ret = {name: sub.threadRetVal for name,sub in self.sub.items()}
        self.log.debug(f"group run end {self.getPos()}")
        self.threadRetVal = ret
        self.threadFinish.set()

    def interrupt(self):
        self.interrupted = True
        for sub in self.sub.values():
            sub.interrupt()

