from pathlib import Path

import yaml
import logging

from roadexec.fn import Config, relpath

#class 

class Shares:
    def __init__(self, fileName:Path=None, prefix:Path=None):
        self.fname = fileName
        self.offset = Path('.') if fileName is None else fileName.parent
        self.prefix = Path('.') if prefix is None else prefix
        self.store = {}
        if self.fname is not None:
            self.loadFile()

    def loadFile(self):
        offset = self.fname.parent
        try:
            with open(self.fname, "r") as fh:   
                f_json = yaml.load(fh, Loader=yaml.SafeLoader)
            for name,value in f_json.items():
                self.store[name] = Path(value)
        except FileNotFoundError:
            pass

    def saveFile(self):
        if self.fname is None:
            return
        stripped = {name: str(value) for name,value in self.store.items()}
        with open(self.fname, "w") as fh:
            yaml.dump(stripped, fh)

    def expose(self, file:Path, name:str):
        self.store[name] = self.prefix / file
        self.saveFile()
    
    def discover(self, name:str):
        if name not in self.store:
            raise NotExposed(name)
        return self.offset / self.store[name]

def load(config:Config):
    try:
        hub = Path(config.get("sharesFile"))
    except KeyError:
        hub = Path("../shares.yaml")
        logging.getLogger("shares").warning("fall back to default shares file:../shares.yaml")

    try:
        prefix = Path(config.get("sharesPrefix"))
    except KeyError:
        prefix = Path('.')
        upCount = 0
        for part in reversed(hub.parts[:-1]):
            if part == '..':
                upCount += 1
            else:
                prefix /= ".."
        prefix = prefix.joinpath(*Path.cwd().parts[-upCount:])

    logging.getLogger("shares").info(f"load shares file:{hub} prefix:{prefix}")

    shares = Shares(hub, prefix)
    return shares


class NotExposed(Exception):
    pass
