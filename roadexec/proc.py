import logging
import os
import pathlib
import re
import signal
import subprocess
import sys
import termios
import threading
import time

import psutil


def _tee(inStream, outName:str, toShell:bool=True, regex:str=None, matches:list=None):
    try:
        with open(outName, 'w') as outStream:
            for raw in iter(inStream.readline, b''):
                line = raw.decode(sys.stdout.encoding)
                if regex:
                    m = re.match(regex, line)
                    if m:
                        matches.append(m)
                if toShell:
                    print(line, end='')
                print(line, end='', file=outStream, flush=True)
    except ValueError:
        pass
    #print("TEE file closed")

class Proc(object):
    def __init__(
        self, args, wd:pathlib.Path, name:str, toShell:bool=True,
        interactive:bool=False, stdoutMatch:str=None, environ:dict=None
    ):
        self.killSequence = [
            (None, None), #run forever
            (signal.SIGINT, 0.5),
            (signal.SIGINT, 5.0),   #double sigint
            (signal.SIGTERM, 5.0),
            (signal.SIGKILL, 5.0)
        ]
        #cmd
        cmd = args
        #stdout scanner
        self.matches = []
        #start process
        self.interactive = interactive
        if interactive:
            #save terminal state
            fn = sys.stdin.fileno()
            self.termattr = termios.tcgetattr(fn)
            #start process
            self.popen = subprocess.Popen(cmd, cwd=wd)
        else:
            if environ is not None:
                env = os.environ.copy()
                env.update(environ)
            else:
                env = None
            self.popen = subprocess.Popen(
                cmd, cwd=wd, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                start_new_session=True, env=env
            )
            #TEE
            fStdout = wd / ('stdout' if name is None else f'{name}.stdout')
            fStderr = wd / ('stderr' if name is None else f'{name}.stderr')
            args = (self.popen.stdout, fStdout, toShell, stdoutMatch, self.matches)
            self.threadStdout = threading.Thread(
                target=_tee, args=args, daemon=True, name=f"TeeStdOut({wd})"
            )
            self.threadStdout.start()
            args = (self.popen.stderr, fStderr, toShell)
            self.threadStderr = threading.Thread(
                target=_tee, args=args, daemon=True, name=f"TeeStdErr({wd})"
            )
            self.threadStderr.start()
        #
        self.process = [psutil.Process(self.popen.pid)]
        self.name = name
        self.log = logging.getLogger(name)
        self.intEvent = threading.Event()

    def __del__(self):
        if self.popen.returncode is None:
            self.log.error("SubProcess was not terminated - please investigate")
        #restore term
        if self.interactive:
            termios.tcsetattr(sys.stdin.fileno(), termios.TCSADRAIN, self.termattr)

    def _update_process(self):
        for proc in self.process:
            try:
                for child in proc.children(recursive=True):
                    if child not in self.process:
                        self.process.append(child)
                        self.log.debug(f"new child process: {child.pid}")
            except psutil.NoSuchProcess:
                pass
        alive = []
        for proc in self.process:
            if proc.is_running():
                alive.append(proc)
            else:
                self.log.debug(f"child disappeared: {proc.pid}")
        self.process = alive

    def signal(self, sig):
        self._update_process()
        for p in self.process:
            try:
                p.send_signal(sig)
            except psutil.NoSuchProcess:
                pass

    def finish(self):
        killIter = iter(self.killSequence)
        killStep = True
        killTime = None
        while True:
            if killTime and killTime < time.time():
                killStep = True
            if killStep:
                try:
                    sig, delay = next(killIter)
                except StopIteration:
                    self.log.warn("kill sequence end")
                    break
                if sig:
                    self.log.info(f"signal: {sig.name} --> {delay}")
                    self.signal(sig)
                killTime = time.time() + delay if delay else None
                killStep = False
            self._update_process()
            if self.popen.returncode is None and self.popen.poll() is not None:
                self.log.debug("main subprocess exited")
                #if killTime is None: #TODO: why start kill sequence when main proc is gone
                #    killStep = True
            if len(self.process) == 0:
                self.log.debug("all childs gone - break")
                break
            delay = max(0, 1.0 if killTime is None else killTime - time.time())
            delay = min(1.0, delay)
            if self.intEvent.wait(timeout=delay):
                self.intEvent.clear()
                self.log.debug("got interrupt - killStep!")
                killStep = True
            #time.sleep(0.5)
        self.popen.poll()
        #restore term
        if self.interactive:
            termios.tcsetattr(sys.stdin.fileno(), termios.TCSADRAIN, self.termattr)
        #
        return self.popen.returncode

    def interrupt(self):
        self.intEvent.set()
