from setuptools import setup, find_packages

setup(
    name='roadrunner',
    version='0.0.0',
    description='tool runner',
    author='Mattis Hasler',
    author_email='mattis@pendor.de',
    packages=[
        'roadrunner',
        'roadrunner.tools',
        'roadrunner.tools.vivado',
        'roadrunner.assets',
        'roadrunner.mod',
        'roadrunner.runner',
        'roadexec'
    ],
    package_data={
        'roadrunner.assets': ['*/*']
    },
    install_requires=['lupa', 'pyyaml', 'psutil'],
    scripts=['bin/roadrunner', 'bin/rr', 'bin/rrscan'],
    data_files=[
        ('share/bash-completion/completions', ['script/bash_completion/rr'])
    ]
)