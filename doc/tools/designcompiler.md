# DesignCompiler

Calls `dc_shell`

| Attribute | Description |
|-----------|-------------|
| tool      | DesignCompiler |
| toplevel  | Name of the toplevel module |
| stdCell   | The standard cell definition |
| sv        | systemverilog files to use; gathered |
| v         | verilog files to use; gathered |
| lib       | libs to use, automatically sorted to Compiled and Uncompiled; gathered |
| libUncompiled | Uncompiled libs to use (*.lib); gathered |
| libCompiled | Compiled libs to use (*.db); gathered |
| flags     | List of flags to use in the gather process |
| clocks    | List of clock definitions |

## Clock

| Attribute | Description |
|-----------|-------------|
| port      |             |
| name      |             |
| period    |             |

The default clock ist:
```yaml
port: clk_i
name: sys_clk
period: 10.0
```

## Standard Cell

| Attribute | Description |
|-----------|-------------|
| corner    | Corner name |
| target_library | List; DC variable |
| symbol_library | Single; DC variable |
| synthetic_library | List; DC variable |

## gather 

The gather process will use the [`gather`](../general/backend.md) function to list values of the attributes: `sv`, `v`, `lib`, `libUncompiled`, `libCompiled`.
Values defined in the attribute `flags` will be given to the gather process as well.
Gathered values are given to the synthesis script as follows:

  * `sv` - `systemverilog_files`
  * `v` - not supported
  * `lib` - sorted to `libUncompiled` and `libCompiled` by endind `.lib` and `.db`
  * `libCompiled` - `libCompiled`
  * `libUncompiled` - `libUncompiled` and `libUncompiledDb` with suffix exchanged.

The standard synthesis script will call `read_lib` on each files from `libUncompiled` and concat `libCompiled` and `libUncompiledDb` to `link_library`.

