# Script

Runs a bash snippet.

| Attribute | Description |
|-----------|-------------|
| tool      | Script      |
| script    | bash snippet to be executed |
| env       | variable definitons prepended  to the script |
| files     | files to be imported to the workdir and made available in the `$files` variable |

This tool will write the file `script` to the workdir and then execute it as `/bin/sh script` from the workdir.

## script

Main script body. It is preseeded with the variable definition from the other attributes.

## env

A dictionary assigning values to variables.
The keys will translate to variables in the script with the dictionary values assigned to it.
The values may be a lists.

## files

list of files to be imported to the workdir.
The new file names (inside workdir) are available in the `$files` variable to the script.

## Example

```yaml
script:
  tool: Script
  script: |
    echo animals: $animals
    echo files: $files
  env:
    animals:
      - kea
      - duck
      - gorilla
      - oran utan
  files:
    - bar
    - foo/first
```