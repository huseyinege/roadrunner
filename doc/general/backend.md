# Backend Functions

In order to ease tool development roadrunner provides some backend functions defined in `rr.py`.
Base parameter to most functions is called `cnf` and is considered the command `node` the tool functions was invoked with.

## travers

Walks the config space following specified \<tags\> and visit each `node` by calling \<fn\> on it.
From a dictionary `node` each attribute will be followed that matches one of the \<tags\>, in List `nodes` each child will be followed.
All nodes (`dict`, `list`, `leaf`) will be visited by calling \<fn\> on them.
Each attribute of a dictionary `node` is being matched against each tag, which is a regular expression.
Upon a match the child `node` will be followed.
Attributes beginning with a `"~"` are considered negated, only being followed when no tag matches.
The visit function \<fn\> must be a callable object accepting a single parameter, which is the `node` to be visited.
The order in which `nodes` are visited can be controlled with <\first_descend\> to be depth-first or breadths-first.

  * `cnf:Node` - a config space node
  * `fn:function` - function to visit a node
  * `tags:[string]=['.*']` - tags to follow
  * `first_descend:boolean=True` - descend to tags before visiting node.
  * `return:None`

## gather

Gathers values of `nodes'` attributes into a linear list, following certain tags, and flags.
The config space will be walked using `travers` and the specified \<tags\>.
For each visited dictionary `node` a dictionary is created assigning the given \<attrs\> the found values.
Values can be retrieved as (location,file) with \<path\> and duplicates can be eliminated with \<unique\>.
In case multiple \<attrs\> are specified both boolean parameters can be a dictionary assigning a different value for each of \<attrs\>.
When values are retrieved for a attribute a second `travers` call is made on the attribute `node` to only extract values accessible by following the \<flags\>.

Example:
```yaml
start:
  include: =..module
  source:
    - test1
    - test2
  header:
    - head1

module:
  source:
    - mod0
    - case1: mod1
    - case2: mod2
    - ~case2: mod2a
  header: modhead

# gather(start_node, ['include'], ['source', 'header'], ['case1'])
# [
#   {
#     'source': ['mod0', 'mod1', 'mod2a']
#     'header': ['modhead']
#   },
#   {
#     'source': ['test1', 'test2']
#     'header': ['head1']
#   }
# ]
```
Returned is a list with one entry for every `node` visited.
Each dictionary has one key for each specified <\attr\>.
The flags matches `case1` so includes `mod1`.
`case2` does not match causing `~case2` to be used and add `mod2a`.

>Note: If only interested in a single list for each \<attr\> have a look at `unite`

  * `cnf:node` - the starting node
  * `tags:[string]` - tags to follow
  * `attrs:[string]` - attributes to gather
  * `flags:[string]` - flags to follow inside an attribute node
  * `path:boolean|{string->boolean}=False` - to retrieve values as paths
  * `unique:boolean|{string->boolean}=True` - eliminate duplicates
  * `return:[{string->[Any]}]` - gathered values

## unite

Takes the output of a gather and for each attribute concats all lists into a single list.
The result is a single dictionary with a list of values for each attribute.
While concatting lists duplicated may be eliminated by setting \<unique\>

  * `lst:[{string->[Any]}]` - the gather result
  * `unique:boolean=True` - eliminate duplicates
  * `return:{string->[Any]}` - single dictionary with a list for every gather attribute

## command_args

Fetches a list of strings passed to the tool over the commandline.
Actually it just fetches `:_run.args`.

  * `return:[string]` - list of command line args

## command_name

Gets the name of the command `node`, which should be equal to the the `nodes's` path without leading `:`.

  * `return:string` - unique command name

## workdir_name

Path to the working directory of the command.
This should be the workdir base directory (`:_setup.workdir_base`) together with the command name.

  * `return:Path` - workdir path

## workdir

Creates and returns the workdir associated with the command.

  * `clear:boolean = False` - if true all files inside the directory will be deleted
  * `return:Path` - workdir path

## Proc (class)

Runs a subprocess for a given set of arguments.
Captures output of the process in files in the workdir.
Can be controlled to send a specific signal sequence for termination.

### Constructor()
  * `args:[string]` - arguments to the program
  * `wd:Path` - working directory
  * `name:string` - internal name, used for file naming
  * `shell:boolean = False` - starting command in a shell
  * `toShell:boolean = True` - output commands output to stdout/stderr

### finish()

Waits for the process to finish returning the returnvalue of the process.
Catches KeyboardInterrupts and initiates a predefined `killSequence`.
The `killSequence` is defined as an attribute:

```python
killSequence:[(int,float)] = [
    (None, None),           #run forever
    (signal.SIGINT, 0.5),
    (signal.SIGINT, 5.0),   #double SIGINT
    (signal.SIGTERM, 5.0),
    (signal.SIGKILL, 5.0)
]
```
When finish is called the first item of the `killSequence` is executed immediately.
In the default case no signal is send and the process is awaited forever.
Catching a `KeyboardInterrupt` will jump to the next item in the `killSequence`.
When the end of the sequence is reached `finish()` will return even if the process is still running, but a warning will be produced.

## proc_run

runs and waits for a subprocess.
Create a `Proc` class and call `finish()` on it

## workdir_import

Copies a list of files to the workdir and returns the paths to the locations in the workdir.
A file location in roadrunner is the tuple (location,file) where \<location\> is the path bound to the `node` the filename comes from and \<file\> is actual value of the `node`.
The \<location\> part is collapsed to a single directory when copying to the workdir.
With the option \<rooted\> the \<location\> can be ignored altogether, copying the file directly to the workdir.
Files are actually hard-linked instead of copied.
If hardlinks are not possible, symlinks are used.

  * `wd:Path` - the workdir
  * `files:[Path]` - files to be imported
  * `rooted:boolean=False` - 

## tool_getattr

Retruns an attribute of a tool configuration.
The location of the value is calculated as

`:_setup.\<tool\>.\<version\>.\<attr\>`

  * `cnf:Node` - the command node
  * `tool:string` - the toolname
  * `attr:string` - attribute name
  * `path:boolean=False` - return value as (location,file) tuple
  * `default:Any=None` - to be returned if the attribute is not found
  * `mklist:boolean=False` - always return a list
  * `return:Any` - the value from the config space

## tool_loadcmd

Writes a script (`\<cmd\>.tool`) to run a given tool's command to the workdir and return and argument list that can be passed to `subprocess.Popen`.

  * `cnf:Node` - the command node
  * `wd:Path` - the workdir
  * `tool:string` - the tool name
  * `cmd` - the tool's command name
  * `version:string=None` - tools version, if None, `"_"` is used.
  * `return:[string]` - arguments to pass to Popen