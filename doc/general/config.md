<!---
# Vodafone Chair Mobile Communication System
# TU Dresden
# 
# email: mattis.hasler@tu-dresden.de
# authors: Mattis Hasler
# notes:
--->
# The Config Space

The config space is the user defined input to roadrunner defining global configuration and project details.

## Structure

The structure of the config space is basically the same expressable by a YAML file.
It is a tree structures with each `node` being one of `dictionary`, `list`, `leaf`.
A `dictionary` is a mapping of `string` to `node`, a `list` is a, well, a list of `nodes` and a `leaf` may hold a simple value of basic types like `string`, `int`, `float`, `boolean`, etc.
In addition there are the special `node` types `link` and `load`.
A `link` is best described as a linux-like-symlink.
It places the targeted `node` at the position of the `link` node itself.
Similar the `load` node loads a YAML files from disk and hooks its root node at inplace of the `load` node.

Roadrunner uses a path system to specify `nodes` in the config space.
The path system allows for global and relative `node` names.
A path is a list of child names separated by `.` similar to a package definition in java or python.
Going up one or multiple levels can be done with multiple consecutive `.`.
For example `..` will go up one level, where `...` goes up two levels.
As a final ingredient, a global path starts with a colon `:` and a relative path with `.`.

```yaml
                    # dict (:)
thing:              # dict (:thing)
  other:            # dict (:thing.other)
    bird: kea.png   # leaf (:thing.other.bird)
  birds:            # list (:thing.birds)
    - eagle         # leaf (:thing.birds.0)
    - vulture       # leaf (:thing.birds.1)
    - =..other.bird # link (:thing.birds.2)
```

This example demonstrates the naming of nodes and the use of link nodes.
The link is specified by starting a string value with `=` followed by an path to a target node.
In this example the relative path starts at `:thing.birds`, raises one level to `:thing` to then descend again to the target node `:thing.other.bird`.

## Config Load

At startup of roadrunner it will load three files to the root of the config space.
These files are:

 * the global config file: `/etc/roadrunner/setup.yaml`
 * the user config files: `~/.config/roadrunner/setup.yaml`
 * and the project config file: `RR`

Only the third file is required.
All resulting `dict` nodes are deeply merged to in the specified order onto a default config space.

All `nodes` created by loading of a file inherit the directory this file was loaded from as their position.
A `string` leaf node can be retrieved from the config space as `file` and will then be interpreted as a file path relative to this position.
The effect is that if a file is referenced from an `RR` file it can be done alwas rlative to the `RR` files.

Additional config files can be loaded by using a load `node`.
A `load` node must start with `+` followed by a relative directory name.
The containing `RR` file will be loaded and linked to the `load` node.

```yaml
### ./RR
module: +mod

### ./mod/RR
files:
  - one
  - two

# roadrunner get :module.files.1 --> two
```

## Lua Script

The config space has an Lua scripting engine embedded allowing inline expression replacement as well as script snippets.

Inline lua expressions can be embedded into string leaf nodes by surounding them with `{}`.
To define a whole node to be a Lua script it must start with `$` or `$$` followed by Lua code.
In case a single `$` is used, the Lua code is actually an expression that will replace the nodes value.
With `$$` the `node` value will be the return value of the given code snippet.
 
```yaml
#all evaluate to "I have 10 fingers"
inline: I have {5+5} fingers
expression: $string.format("I have %d fingers", 5+5)
script: $$return string.format("I have %d fingers", 5+5)
```

The value of a `node` calculated by Lua can be `dicts` and `lists` by using Lua's table structures:

```lua
luascript: |
$$a = {}
  for i = 1, 10 do a["attr" .. i] = "foo" .. i end
  return a
```
>NOTE:
>The `|` is a YAML specific construct introducing a multiline string that is stripped of the leading whitespaces to match the YAML indention rules.

will produce:
```yaml
luascript:
  attr1: foo1
  attr2: foo2
  ...
```

### Variables Substitution

Whenever the Lua interpreter is invoked it will fill its the global name space with some variables and functions.
For the global variables the config space will be travered upwards from the position of the Lua script.
On each level the `.vars` key is accessed if available and all child leaf nodes are added to the Lua environment as global variables.

```yaml
vars:
  bird: cockatoo
  monkey: mandrill

node:
  vars:
    bird: kea
  subitm:
    value: The bird is a {bird}, while the monkey is a {monkey}
#roadrunner get :node.subitm.value --> The bird is a kea, while the monkey is a mandrill
```

In this example `:node.vars.bird` overwrites `:vars.bird` in the global variable `bird` because it is on a higher level in the config space tree.

### Native Functions

Apart from the variables loaded from the config space, roadrunner defines some functions that are available to the Lua interpreter.
These are:

  * `getversion()` - returns roadrunner version string
  * `getwd(node)` - returns the workdir of a command node given as `node`
  * `gather(node, targets, tags)` - returns a list of values (missing description)
  * `node` (variable) - current node
  * `parent` (variable) - current node's parent node
  * `get(node, path)` - get node at given path; starting at node if path is relative
  * `getval(node, path)` - get the value of the node at given path

>NOTE
>I know that the native functions are kind of useless right now.
>But there is room for improvement