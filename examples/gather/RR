# The gather command is one of the most important core features of roadrunner
# It is available as a lua binding to the user directly but more importantly it
# is used by many tool implmentations.
# The purpose of gather is to collect a list of values of a specific type from a
# tree structure
# The three parameter to control the value extraction are:
# * tags
# * attr
# * flags
# Since many tools expose these parameters to the user to a certain degree it is
# important to understand their meaning
#
# Tags
# Gather will start traversing the tree at a given node. It will first look for
# subnodes that match one of the <tags>. When finding a matching subnode it will
# recursively descend into that node and continue its search. Usually, the tags
# parameter defaults to ['inc', 'include'].
# A flag system applies and allows parametrized tree traversal.
# See also >Key matching<
#
# Attr
# While technically the <attr> parameter can be a list, we will consider it to 
# be a single string for simplicity reasons. This is the name of the values we 
# are interested in. It may be 'sv' for example when searing for systemverilog
# source files. When gather finds a subnode matching <attr> with its name it
# will, in the simplest case, add the value of the subnode to the result list or
# extend the result list by the whole subnode if it is a list itself.
# In case the matching subnode is a Dictionary the <flags> parameter comes into
# play. A >Key matching< will be performed with the flags on the current subtree
#
# Flags
# Flags describe the keys that are selected within a matched <attr> node.
# Often flags can be specified by the user with a tool attribute.
# For simplicity reasons the flags are often also used as tags.
# However, the tag set and flag set are not completely the same. Tags usually
# include the 'inc' and 'include' tags.
#
# Key Matching
# To see if a subnode matches a key set the subnode name is compared to each key
# . A subnode name may have a leading '~', which means that it matches when the
# following identifier is NOT in the key set. For example '~A' will match
# {'B', 'C'} for 'A' is not in this set.
# This allows for example to define different subtrees for FPGA compilation
# ('FPGA') and any other compilation ('~FPGA')
# The empty negation ('~') will match and key set

foo:
  inc:
    - =...foo1
    - =...foo2

#classic node not using flags
foo1:
  num:
    - thrityone
    - one hundred

#flag using node
foo2:
  num:            #attribute node
    odd:          #flag
      - three
      - seven
    ~even:        #inverted flag
      - eleven
      - 1337
    even:         #flag
      - two
      - twenty
    ~: one        #"always"


tests:
  # no flags included - will match with '~' and '~even'
  t1: $gather(get(parent, "..foo"), "num", "inc")
  # will match '~' and 'even'
  t2: $gather(get(parent, "..foo"), "num", "inc", "even")

# try: rr get :tests.t1