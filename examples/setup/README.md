# Setup 

In order to use `roadrunner` it has to be hooked into your path variables.
There is a `setup.py` so you could use `setuptools` to install it.
But if you intent to actually work on roadrunner itself it makes more sense to just add it manually.

## Infect Shell

The script `infect_shell.sh` will add the neede values to `$PATH` and `$PYTHONPATH`.
After sourcing it with `. path/to/infect_shell.sh` the `roadrunner` command should be available on your shell. There is also a symlink `rr` for quicker access.

## Tool Setup

If you run `rr get :` in this directory you will see a bunch of things in the configspace already even with no `RR` file present.
Especially, there is a node `:_setup` containing global config.
For example the Vivado tool expects there to be some values about the location of binaries.
Roadrunner will load some gloabal config files at startup, which is supposed to populate the `:_setup` node.
The config files merged on the configspace are:

* `/etc/raodrunner/setup.yaml`
* `~/.config/roadrunner/setup.yaml`

## Vivado setup

The Vivado tool expects the binary location of several tool (`xsim`, `xelab`, `vivado`, etc.) to be specified in the `:_setup` section. For each binary a bash
script must be specified that prepares the environment and runs the binary.
In this very directory are some example config files:

* vivado already in path: `plain_vivado.yaml`
* vivado to be loaded by environment modules: `modules_vivado.yaml`

These files might help to compose your personal `~/.config/roadrunner/setup.yaml` or the machine's `/etc/roadrunner/setup.yaml`
