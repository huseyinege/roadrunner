{ nixpkgs ? <nixpkgs>, system ? builtins.currentSystem }:
with import nixpkgs { inherit system; };

let
  pypkg = python-packages: with python-packages; [
    pyyaml
    lupa
    psutil
  ]; 
  py = python3.withPackages pypkg;
in mkShell {
  packages = [
    py
    yosys
    clang
    yamllint
    verilog
    svls
  ];
  nativeBuildInputs = with pkgs; [
      pkg-config
  ];
  shellHook = ''
    export PATH=$PATH:$PWD/bin
    export PYTHONPATH=$PWD
  '';
}
