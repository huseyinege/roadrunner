import unittest
from roadrunner import config, run

class TestVivado(unittest.TestCase):
    def setUp(self):
        self.cnf = config.makeConfigVal(run.CONFIG)
        self.cnf.merge(config.fromstr(f"""
          _setup:
            workdir_base: rtest/rrun/{self.id()}
            result_base: rtest/rres/{self.id()}
          _run:
            command: =:run
        """, "tests"))

    def test_glbl(self):
        """
        Tests using the glbl option in Vivado sim
        """
        self.cnf.merge(config.fromstr(f"""
          run:
            tool: Vivado.sim
            sv: test_vivado.sv
            toplevel: Test
            options:
              - use_glbl
            lib:
              - unisims_ver
        """, "tests"))
        run.run(self.cnf)
        #todo check return value??
        # we cannot check the return value, so if nothing bad happend, we good?

